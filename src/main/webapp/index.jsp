<!DOCTYPE html>
<html>
	<head>
		<title>Skinny &ndash; A 30-Day Retrospective</title>
		
		<link rel="stylesheet" type="text/css" href="styles/fonts.css" />
		<link rel="stylesheet/less" type="text/css" href="styles/all.less" />
		
		<script type="text/javascript">
			var API_ROOT = "<%= request.getContextPath() %>/api/";
			var COMET_ROOT = "<%= request.getContextPath() %>/async/";
		</script>
		
		<script type="text/javascript" src="scripts/jquery/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="scripts/jquery/json2.js"></script>
		<script type="text/javascript" src="scripts/org/cometd.js"></script>
		<script type="text/javascript" src="scripts/jquery/jquery.cometd.js"></script>
		
		<script type="text/javascript" src="scripts/d3.v2.js"></script>
		<script type="text/javascript" src="scripts/accounting.min.js"></script>
		<script type="text/javascript" src="scripts/underscore-min.js"></script>
		<script type="text/javascript" src="scripts/backbone.js"></script>
		<script type="text/javascript" src="scripts/less-1.3.0.min.js"></script>
		
		<script type="text/javascript" src="scripts/CometClient.js"></script>
		<script type="text/javascript" src="scripts/models.js"></script>
		<script type="text/javascript" src="scripts/views.js"></script>
	</head>
	
	<body>
		<div class="container">
			<header>
				<h1>Skinny</h1>
				<div>A <%= vc.bjn.skinny.tailor.data.ga.DateFilter.DEFAULT_FILTER_DAYS %>-Day Retrospective</div>
			</header>
			
			<section class="development">
				<h2><span>Development</span></h2>
				
				<div class="articles">
					<article class="codeFreeze countdown">
						<h3>Code Freeze</h3>
						<figure class="numeric">
							<span class="value"></span>
							<span class="note"> days</span>
						</figure>
					</article>
					
					<article class="release countdown">
						<h3>Release</h3>
						<figure class="numeric">
							<span class="value"></span>
							<span class="note"> days</span>
						</figure>
					</article>
					
					<article class="versionMatrix">
						<h3>Versions</h3>
						<figure class="table">
							<dl></dl>
						</figure>
					</article>
					
					<article class="codebase">
						<h3>Codebase</h3>
						<figure class="bars">
						</figure>
					</article>
				</div>
			</section>
			
			<section class="installation">
				<h2><span>Installation</span></h2>
				
				<div class="articles">
					<article class="attempts">
						<h3><span>Attempts</span></h3>
						<figure class="numeric">
							<span class="value"></span>
						</figure>
					</article>
					
					<article class="attemptsByMethod">
						<h3>Method Share</h3>
						<figure class="pie">
							<figcaption></figcaption>
						</figure>
					</article>
					
					<article class="timeByMethod">
						<h3>Method Time</h3>
						<figure class="bars"></figure>
						<!--<figure class="numeric manual">
							<h4>Manual</h4>
							<span class="value"></span>
							<span class="note"> sec</span>
						</figure>
						<figure class="numeric autoInstall">
							<h4>Auto-Install</h4>
							<span class="value"></span>
							<span class="note"> sec</span>
						</figure>
						<figure class="numeric autoUpdate">
							<h4>Auto-Update</h4>
							<span class="value"></span>
							<span class="note"> sec</span>
						</figure>-->
					</article>
					
					<article class="manualTime">
						<h3>Manual Time</h3>
						<figure class="bars">
						</figure>
					</article>
					
				</div>
			</section>
			
			<section class="attending">
				<h2><span>Attending</span></h2>
				
				<div class="articles">
					<article class="joinSuccesses">
						<h3><span>Joins</span></h3>
						<figure class="numeric"></figure>
					</article>
					
					<article class="joinFailureRate">
						<h3>Join Failure</h3>
						<figure class="pie">
							<figcaption><strong class="error"></strong> failure</figcaption>
						</figure>
					</article>
				
					<article class="uiLoadTime">
						<h3>Page Load Time</h3>
						<figure class="histogram"></figure>
					</article>
					
					<article class="joinTime">
						<h3>Join Time</h3>
						<figure class="histogram"></figure>
					</article>
					
					<hr />
					
					<article class="os">
						<h3>Operating System</h3>
						<figure class="pie">
							<figcaption></figcaption>
						</figure>
					</article>
					
					<article class="browser">
						<h3>Browser</h3>
						<figure class="pie">
							<figcaption></figcaption>
						</figure>
					</article>
					
					<article class="platformSupport">
						<h3>Platform Support</h3>
						<figure class="pie">
							<figcaption><strong class="error"></strong> unsupported</figcaption>
						</figure>
					</article>
				</div>
			</section>
			
			<section class="behavior">
				<h2><span>Behavior</span></h2>
				
				<div class="articles">
					<article class="moderationRatio">
						<h3>Moderators</h3>
						<figure class="pie">
							<figcaption>
								<strong></strong> moderators
							</figcaption>
						</figure>
					</article>
					
					<article class="authenticatedRatio">
						<h3>Logged In</h3>
						<figure class="pie">
							<figcaption>
								<strong></strong> logged in
							</figcaption>
						</figure>
					</article>
					
					<hr />
					
					<article class="elementUsage">
						<h3>Feature Usage</h3>
						<figure class="iconTable">
							<dl></dl>
						</figure>
					</article>
				</div>
			</section>
		</div>
		
		<script type="text/javascript" src="scripts/fetchDataAndRender.js"></script>
		<script type="text/javascript">
			main();
		</script>
	</body>
</html>
