window.CometClient = (function($) {
	var cometd = $.cometd;
	
	// Disconnect when the page unloads
	$(window).unload(function() {
		cometd.disconnect(true);
	});

	var CometClient = function(connectionUrl){
		
		var self = this;
		var _connected = false;
		
		var queuedSubscriptions = [];
		var queuedPublishes = [];
		
		this.onConnectionEstablished = function() {
			console.info('CometD Connection Established');
		};
	
		this.onConnectionBroken = function() {
			console.error('CometD Connection Broken');
		};
	
		this.onConnectionClosed = function() {
			console.info('CometD Connection Closed');
		};
	
		// Function that manages the connection status with the Bayeux
		// server
		function _metaConnect(message) {
			if (cometd.isDisconnected()) {
				_connected = false;
				self.onConnectionClosed();
				return;
			}
	
			var wasConnected = _connected;
			_connected = message.successful === true;
			if (!wasConnected && _connected) {
				self.onConnectionEstablished();
			} else if (wasConnected && !_connected) {
				self.onConnectionBroken();
			}
		}
	
		// Function invoked when first contacting the server and
		// when the server has lost the state of this client
		function _metaHandshake(handshake) {
			if (handshake.successful === true) {
				cometd.batch(function() {
					var queuedActions = queuedSubscriptions.length + queuedPublishes.length;
					
					queuedSubscriptions.forEach(function(queuedSubscription){
						//cometd.subscribe(queuedSubscription.topic, queuedSubscription.onMessage);
						queuedSubscription();
					});
					
					queuedSubscriptions = [];
					
					queuedPublishes.forEach(function(queuedPublish){
						queuedPublish();
						//cometd.publish(queuedSubscription.topic, queuedSubscription.message);
					});
					
					queuedPublishes = [];
					
					console.info('Handled '+queuedActions+" queued actions.");
					
					/*cometd.subscribe('/hello', function(message) {
						$('#body').append(
								'<div>Server Says: '
										+ message.data.greeting
										+ '</div>');
					});*/
					// Publish on a service channel since the message is
					// for the server only
					/*cometd.publish('/service/hello', {
						name : 'World'
					});*/
				});
			} else {
				console.error('Handshake failed', handshake);
			}
		}
	
		/**
		 * @param url absolute url to Bayeux endpoint, as defined in Servlet mapping for the CometdServlet in web.xml
		 */
		this.connect = function(url){
			cometd.configure({
				url : url,
				logLevel : 'info'
			});
			
			cometd.addListener('/meta/handshake', _metaHandshake);
			cometd.addListener('/meta/connect', _metaConnect);
			
			console.debug("Connecting to Comet server at "+url);
			
			cometd.handshake();
		};
		
		this.disconnect = function(){
			cometd.disconnect();
		};
		
		/* onMessage signature: function(message) */
		//TODO does not return a subscription handle if the connection was not established at invocation time
		this.subscribe = function(topic, onMessage, scope){
			var doSubscribe = function(){
				return cometd.subscribe(topic, _.bind(onMessage, scope));
			};
			if(_connected){
				return doSubscribe;
			} else {
				queuedSubscriptions.push(doSubscribe);
			}
		};
		
		this.unsubscribe = function(subscriptionHandle){
			cometd.unsubscribe(subscriptionHandle);
		};
		
		this.publish = function(topic, message){
			var doPublish = function(){
				return cometd.publish(topic, message);
			};
			if(_connected){
				doPublish();
			} else {
				queuedPublishes.push(doPublish);
			}
		};
		
		if(connectionUrl){
			this.connect(connectionUrl);
		}
	};
	
	return CometClient;
})(jQuery);
