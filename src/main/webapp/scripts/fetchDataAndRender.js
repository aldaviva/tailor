function main(){

	window.cometClient = new CometClient(location.protocol + "//" + location.host + COMET_ROOT);
	
	(function(){
		/*** Models ***/
		var joinSuccessModel = new model.Numeric({ id: 'attending/joinCount' }, { isLive: true }).schedule();
		var installationAttemptsModel = new model.Numeric({ id: 'installation/attempts' }, { isLive: true }).schedule();
		var avgUiLoadTimeModel = new model.Numeric({ id: 'attending/avgUiLoadTime' }, { isLive: true, units: "s" }).schedule();
		var uiLoadTimeModel = new model.WeightedMap({ id: 'attending/uiLoadTime' },{ units: "s" }).schedule();
		var joinTimeModel = new model.WeightedMap({ id: 'attending/joinTime' },{ units: "s" }).schedule();
		
		/*** Views ***/
		var joinSuccessView = new view.Numeric({
			el: $('.attending .joinSuccesses figure')[0],
			model: joinSuccessModel
		});
		
		var installationAttemptsView = new view.Numeric({
			el: $('.installation .attempts figure')[0],
			model: installationAttemptsModel
		});
		
		/*var uiLoadTimeView = new view.Numeric({
			el: $('.attending .uiLoadTime figure')[0],
			model: avgUiLoadTimeModel,
			decimalPlaces: 1,
			divisor: 1000
		});*/
		
		var uiLoadTimeView = new view.Histogram({
			el: $('.attending .uiLoadTime figure')[0],
			model: uiLoadTimeModel,
			divisor: 1000,
			decimalPlaces: 1
		});
		
		var joinTimeView = new view.Histogram({
			el: $('.attending .joinTime figure')[0],
			model: joinTimeModel,
			divisor: 1000,
			decimalPlaces: 1
		});
		
	})();
	
	$.get(API_ROOT + 'attending/joinCount', function(successCount){
		$.get(API_ROOT + 'attending/uiLoadCount', function(attemptCount){
			var failureFigure = $('.attending .joinFailureRate figure');
			
			var failureCount = attemptCount - successCount;
			var failureRate = failureCount / attemptCount;
			
			failureFigure.prepend(makePieChart([successCount, failureCount], ['success', 'failure']));
			failureFigure.find('.error').text(accounting.formatNumber(failureRate*100, 0)+'%');
		});
	});
	
	var installationMethodLabels = {
		manual: "manual",
		autoUpdate: "auto-update",
		autoInstall: "auto-install"
	};

	/*$.get(API_ROOT+"installation/attempts", function(attemptsCount){
		var label = accounting.formatNumber(attemptsCount, 0);
		$('.installation .attempts figure .value').text(label);
	});*/
	
	$.get(API_ROOT + "attending/platformSupportedRatio", function(data){
		var el = $('.attending .platformSupport figure');
		
		var labels = _.keys(data);
		var values = _.values(data);
		
		el.prepend(makePieChart(values, labels));
		
		var unsupportedFraction = (data['unsupported'] / (data['unsupported'] + data['supported']));
		var label = accounting.formatNumber(100 * unsupportedFraction, 0) + '%';
		el.find('strong').text(label);
	});
	
	$.get(API_ROOT + 'attending/browserShare', function(data){
		if(data['other'] !== undefined){
			delete data['other'];
		}
		var labels = _.keys(data);
		var values = _.values(data);
		
		var figure = $('.attending .browser figure');
		figure.prepend(makePieChart(values, labels));
		
		var caption = figure.find('figcaption');
		_(labels).chain()
			.sortBy(function(label, index){
				return -values[index];
			})
			.each(function(label, index){
				if(index > 0){
					caption.append("&nbsp;&middot;&nbsp;");
				}
				var color = figure.find('svg .'+label).css('fill');
				caption.append($('<span>').css('color', color).text(label));
			});
	});
	
	$.get(API_ROOT + 'attending/osShare', function(data){
		
		var labels = [];
		var values = _(data).chain()
			.groupBy(function(item, key){ return key.split(" ")[0]; })
			.pick(['win', 'mac'])
			.tap(function(obj){
				labels = _(obj).keys();
			})
			.map(function(item, key){
				return _(item).reduce(function(old, curr){
					return old + curr;
				}, 0);
			})
			.value();

		
		/*var labels = _.keys(data);
		var values = _.values(data);*/
		
		var figure = $('.attending .os figure');
		figure.prepend(makePieChart(values, labels));
		
		var caption = figure.find('figcaption');
		caption.append($('<span>').addClass('win').text('win'))
			.append('&nbsp;&middot;&nbsp;')
			.append($('<span>').addClass('mac').text('mac'));
	});
	
	$.get(API_ROOT + 'installation/manualTime', function(data){
		var figure = $('.installation .manualTime figure');
		
		var stats = _(data).chain()
			.values()
			.reduce(function(prevStats, currItem){ return { 
				total: prevStats.total + currItem.count,
				max: Math.max(prevStats.max, currItem.avgTime),
				avgNumerator: prevStats.avgNumerator + (currItem.count * currItem.avgTime)
			};}, { total: 0, max: 0, avgNumerator: 0 })
			.tap(function(finalStats){
				finalStats.average = finalStats.avgNumerator / finalStats.total;
				delete finalStats.avgNumerator;
			})
			.value();
		
		_(data).chain()
			.map(function(osData, osName){ return { os: osName, millis: osData.avgTime, count: osData.count };})
			.sortBy(function(item){ return -item.millis; })
			.each(function(item, index){
				var valueFraction = item.millis/stats.max;
				var row = $('<div>');
				figure.append(row);
				row.append($('<div>')
						.addClass('bar')
						.append($('<div>')
								.addClass('category'+(index%8))
								.css('width', valueFraction * 100 + '%')));
				row.append($('<span>')
						.addClass('key')
						.text(item.os));
				row.append($('<span>')
						.addClass('value')
						.text(accounting.formatNumber(item.millis/1000, 0))
						.append($('<span>')
								.addClass('unit')
								.text('s')));
			});
		
		figure.append($('<div>')
				.addClass('footer')
				.append($('<span>')
						.addClass('key')
						.text('avg.'))
				.append($('<span>')
						.addClass('value')
						.text(accounting.formatNumber(stats.average/1000, 0))
						.append($('<span>')
								.addClass('unit').text('s'))));
		
		/*_(data).each(function(value, key){
			dl.append($('<dt>').text(key.substr(0,1).toUpperCase() + key.substr(1)+':'));
			dl.append($('<dd>').text(accounting.formatNumber(value/1000, 1)+'s'));
		});*/
	});
	
	/*$.get(API_ROOT + 'attending/avgUiLoadTime', function(data){
		$('.attending .uiLoadTime figure .value').text(accounting.formatNumber(data/1000, 1));
	});*/
	
	$.get(API_ROOT + 'attending/avgJoinTime', function(avgJoinTime){
		var figure = $('.attending .joinTime figure');
		figure.find('.value').text(accounting.formatNumber(avgJoinTime/1000, 1));
	});
	
	$.get(API_ROOT + 'installation/attemptsByMethod', function(data){
		var figure = $('.installation .attemptsByMethod figure');
		
		var labels = _.keys(data);
		var values = _.map(data, function(methodData){
			return methodData.success + methodData.failure;
		});
		
		figure.prepend(makePieChart(values, labels));
		
		var caption = figure.find('figcaption');
		_(labels).each(function(label, index){
			if(index > 0){
				caption.append("&nbsp;&middot;&nbsp;");
			}
			
			caption.append($('<span>').addClass("category"+(index%8)).text(installationMethodLabels[label]));
		});
	});
	
	$.get(API_ROOT + 'installation/timeByMethod', function(data){
		var figure = $('.installation .timeByMethod figure');
		
		var stats = _(data).chain()
			.values()
			.reduce(function(prevStats, currItem){ return { 
				total: prevStats.total + currItem,
				max: Math.max(prevStats.max, currItem)
			};}, { total: 0, max: 0 })
			.value();
		
		_(data).chain()
			.map(function(time, method){ return { method: method, time: time };})
			.sortBy(function(item){ return -item.time; })
			.each(function(item, index){
				var valueFraction = item.time/stats.max;
				var row = $('<div>');
				figure.append(row);
				row.append($('<div>')
						.addClass('bar')
						.append($('<div>')
								.addClass('category'+(index%8))
								.css('width', valueFraction * 100 + '%')));
				row.append($('<span>')
						.addClass('key')
						.text(installationMethodLabels[item.method]));
				row.append($('<span>')
						.addClass('value')
						.text(accounting.formatNumber(item.time/1000, 0))
						.append($('<span>')
								.addClass('unit')
								.text('s')));
			});
		
		/*article.find('.manual .value').text(accounting.formatNumber(data['manual']/1000, 1));
		article.find('.autoInstall .value').text(accounting.formatNumber(data['autoInstall']/1000, 1));
		article.find('.autoUpdate .value').text(accounting.formatNumber(data['autoUpdate']/1000, 1));*/
	});
	
	$.get(API_ROOT + 'behavior/moderationRatio', function(data){
		var labels = _.keys(data);
		var values = _.values(data);
		
		var figure = $('.behavior .moderationRatio figure');
		
		figure.prepend(makePieChart(values, labels));

		var moderatorFraction = data['moderator'] / (data['moderator'] + data['non-moderator']);
		figure.find('figcaption strong').text(accounting.formatNumber(moderatorFraction * 100, 0)+'%');
	});
	
	$.get(API_ROOT + 'behavior/authenticatedRatio', function(data){
		var labels = _.keys(data);
		var values = _.values(data);
		
		var figure = $('.behavior .authenticatedRatio figure');
		figure.prepend(makePieChart(values, labels));
		
		var loggedInFraction = data['authenticated'] / (data['authenticated'] + data['anonymous']);
		figure.find('figcaption strong').text(accounting.formatNumber(loggedInFraction * 100, 0)+'%');
	});
	
	$.get(API_ROOT + 'development/release', function(release){
		$('.development .codeFreeze .value').text(release.daysUntilCodeFreeze);
		$('.development .release .value').text(release.daysUntilRelease);
		$('.development .release h3').text(release.name);
		
		if(Math.abs(release.daysUntilCodeFreeze) == 1){
			$('.development .codeFreeze .note').text(' day');
		}
		
		if(Math.abs(release.daysUntilRelease) == 1){
			$('.development .release .note').text(' day');
		}
		
		$('.development .release').toggleClass('late', release.daysUntilRelease < 0);
	});
	
	$.get(API_ROOT + 'development/versionMatrix', function(versions){
		var dl = $('.development .versionMatrix dl');
		
		_(['master', 'e', 'stage', 'prod']).each(function(key){
			dl.append($('<dt>').text(key));
			if(versions[key+'Version']){
				var valueArr = versions[key+'Version'].split('.');
				var buildNum = valueArr.pop();
				
				dl.append($('<dd>')
						.text(valueArr.join('.')+'.')
						.append($('<span>')
								.addClass('build')
								.text(buildNum)));
			} else {
				dl.append($('<dd>').text('-'));
			}
		});
	});
	
	var elementUsageLabels = {
		usedPreview: "Preview Used",
		previewTimedOut: "Preview Timed Out",
		changedDefaultDevices: "Devices Changed",
		usedKeyboardAudioMute: "Hotkey Mic",
		usedKeyboardVideoMute: "Hotkey Camera",
		collapsedParticipantPanel: "Sidebar Collapsed",
		closedSelfView: "Self View Closed",
		openedSettings: "Settings Opened",
		sawLeavePanel: "Leave Panel Opened",
		leftUsingPanel: "Leave Panel Used",
		sawLayoutPanel: "Layout Panel Opened",
		changedLayoutUsingPanel: "Layout Panel Used",
		usedRosterMuteSelf: "Roster Muted Self",
		usedRosterMuteOthers: "Roster Muted Other",
		expandedRosterPanel: "Roster Row Expanded",
		droppedParticipant: "Dropped Participant",
		clickedOnEditName: "Name Changed"
	};
	
	$.get(API_ROOT + 'behavior/elementUsage', function(data){
		$.get(API_ROOT + 'attending/joinCount', function(successfulJoins){
			var dl = $('.behavior .elementUsage figure dl');
			_(data).chain()
				.map(function(timesUsed, id){ return {
					timesUsed: timesUsed,
					id: id,
					label: (elementUsageLabels[id] !== undefined) ? elementUsageLabels[id] : id
				};})
				.sortBy(function(item){ return item.label; })
				.each(function(item){
					var timesUsedFraction = item.timesUsed/successfulJoins;
					dl.append($('<dt>').text(item.label).addClass(item.id)
						.append($('<span>').addClass('icon primary'))
						.append($('<span>').addClass('icon secondary')));
					dl.append($('<dd>').text(accounting.formatNumber(timesUsedFraction * 100, 0)+'%'));
				});
		});
	});
	
	$.get(API_ROOT + 'development/linesOfCode', function(data){
		var figure = $('.development .codebase figure');
		
		var stats = _(data).chain()
			.values()
			.reduce(function(prevStats, currItem){ return { 
				total: prevStats.total + currItem,
				max: Math.max(prevStats.max, currItem)
			};}, { total: 0, max: 0})
			.value();
		
		_(data).chain()
			.map(function(lines, lang){ return { lang: lang, lines: lines };})
			.sortBy(function(item){ return -item.lines; })
			.each(function(item, index){
				var valueFraction = item.lines/stats.max;
				var row = $('<div>');
				figure.append(row);
				row.append($('<div>')
					.addClass('bar')
					.append($('<div>')
						.addClass('category'+(index%8))
						.css('width', valueFraction * 100 + '%')));
				row.append($('<span>')
					.addClass('key')
					.text(item.lang));
				row.append($('<span>')
					.addClass('value')
					.text(accounting.formatNumber(item.lines/1000, 0))
					.append($('<span>')
						.addClass('unit')
						.text('k')));
			});
		
		figure.append($('<div>')
			.addClass('footer')
			.append($('<span>')
				.addClass('key')
				.text('Total'))
			.append($('<span>')
				.addClass('value')
				.text(accounting.formatNumber(stats.total/1000, 0))
				.append($('<span>')
					.addClass('unit').text('k'))));
	});
	
	
	
	function makePieChart(values, labels){
		var width = 140,
		    height = 140,
		    padding = 0,
		    outerRadius = Math.min(width, height) / 2 - padding,
		    innerRadius = 0,
		    pieLayout = d3.layout.pie();
		    arc = d3.svg.arc().innerRadius(innerRadius).outerRadius(outerRadius);
		
		var startAngle = Math.PI / 6;
		
		pieLayout.sort(d3.ascending)
			.startAngle(startAngle)
			.endAngle(2 * Math.PI + startAngle);
		
		var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
		
		var vis = d3.select(svg)
			.data([values])
	  	    .attr("viewBox", [0, 0, width, height].join(','));
	  	
	  	var arcs = vis.selectAll("g.arc")
	  	    .data(pieLayout)
	  	  .enter().append("g")
	  	    .attr('class', function(d, i) { return "arc category" + (i%8) + " " + labels[i].toLowerCase(); })
	  	    .attr("transform", "translate(" + (outerRadius+padding) + "," + (outerRadius+padding) + ")");
	  	
	  	arcs.append("path")
	  	    .attr("d", arc);
	  	
	  	return svg;
	}
	
	window.setInterval(window.location.reload, 1000*60*60);
}