(function(scope){
	
	scope.mixins || (scope.mixins = {});
	
	var baseModel = Backbone.Model.extend({
		urlRoot: API_ROOT,
		
		initialize: function(){
			_.bindAll(this);
			_.defaults(this.options, {
				isLive: false,
				units: ""
			});
		},
		/* By default, ids are urlencoded, but we want to allow slashes in ids to pass through */
		url: function(){
			return baseModel.__super__.url.call(this).replace(/%2F/ig, '/');
		},
		hasUnits: function(){
			return this.options.units && this.units.options.length;
		},
		getData: function(){
			return this.get('data');
		},
		setData: function(value){
			this.set({ value: value });
		},
		schedule: function(arg0){
			_.defer(this.fetch);
			
			if(this.options.isLive){
				var topic = arg0 || '/events/'+this.id;
				cometClient.subscribe(topic, function(message){
					this.set(this.parse(message.data, message, true));
				}, this);
				console.info('subscribed to '+topic);
				
			} else {
				var interval = arg0 || 5 * 60 * 1000;
				window.setInterval(this.fetch, interval);
			}
			
			return this;
		}
	});
	
	scope.Numeric = baseModel.extend({
		parse: function(data, response, isLive){
			return { data: data };
		}
	});
	
	scope.Map = baseModel.extend({
		parse: function(data){
			return { data: data };
		},
		getValuesSortedByKey: function(isDesc){
			return _(this.getData()).sortBy(function(v, k){
				return k * (isDesc ? -1 : 1);
			});
		},
		each: function(callback, context){
			_(this.getData()).each(callback, context);
		},
		sum: function(){
			return _(this.getData()).reduce(function(prev, curr){
				return prev + curr;
			}, 0);
		},
		average: function(){
			return this.sum() / _(this.getData()).size();
		},
		max: function(){
			return _(this.getData()).max();
		}
	});
	
	scope.WeightedMap = scope.Map.extend({
		sum: function(){
			return _(this.getData()).reduce(function(prev, curr){
				return prev + (curr.count * curr.value);
			}, 0);
		},
		average: function(){
			var stats = _(this.getData()).reduce(function(prev, count, bucket){
				return {
					numerator: prev.numerator + (count * bucket),
					denominator: prev.denominator + count
				};
			}, { numerator: 0, denominator: 0 });
			
			return stats.numerator / stats.denominator;
		},
		getBucket: function(value){
			 return _(this.getData()).chain().keys().map(function(x){
				    return x - value;
				}).filter(function(x){
				    return x < 0;
				}).max().value() + value;
		}
	});
	
	
})(window.model || (window.model = {}));