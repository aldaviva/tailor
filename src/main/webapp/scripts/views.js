(function(scope){
	
	var baseView = Backbone.View.extend({
		
		initialize: function(){
			_.bindAll(this);
			_.defaults(this.options, {
				commas: true,
				decimalPlaces: 0,
				suffix: '',
				divisor: 1
			});
			
			this.model.on('change', this.render, this);
		},
		formatValue: function(value){
			return accounting.formatNumber(value/this.options.divisor, this.options.decimalPlaces, this.options.commas ? ',' : '') + this.options.suffix;
		}
	});
	
	scope.Numeric = baseView.extend({
		initialize: function(){
			scope.Numeric.__super__.initialize.call(this);
		},
		valueEl: null,
		render: function(){
			if(!this.valueEl){
				this.valueEl = this.make("span", {"class": "value"});
				this.$el.append(this.valueEl);
				if(this.model.hasUnits()){
					this.$el.append(this.make("span", {"class":"note"}, this.model.options.units));
				}
			}
			
			$(this.valueEl).text(this.formatValue(this.model.getData()));
			
			return this.el;
		}
	});
	
	scope.Histogram = baseView.extend({
		bars: null,
		yScale: null,
		captionEl: null,
		initialize: function(){
			scope.Histogram.__super__.initialize.call(this);
			
			var barsArea = this.make("div", {"class":"bars"});
			this.$el.append(barsArea);
			
			this.yScale = d3.scale.linear().range(["0%", "100%"]);
			this.bars = d3.select(barsArea).selectAll('div').data([4]);
			
			this.$el.append(this.make("figcaption", null, "<strong></strong> avg."));
			this.captionEl = this.$('figcaption strong');
		},
		render: function(){
			var max = this.model.max();
			var avg = this.model.average();
			this.yScale.domain([0, max]);
			
			var avgBucket = this.model.getBucket(avg)/this.options.divisor;
			console.log("avgBucket = "+avgBucket);
			var values = this.model.getValuesSortedByKey();
			
			this.bars = this.bars.data(values);
			
			this.bars.enter()
				.append("div")
					.attr("class", function(d, i){
						if(i == avgBucket){
							return "average";
						}
					})
					.append("span")
						.text(_.bind(function(d, i){
							return i;
						}, this));
			
			this.bars.exit()
				.remove();
			
			this.bars
				.style("height", _.bind(function(d){
					var height = this.yScale(d);
					return height;
				}, this));
			
			this.captionEl.text(this.formatValue(avg)+this.model.options.units);
			
			return this.el;
		}
		
	});
	
})(window.view || (window.view = {}));