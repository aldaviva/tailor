#!/usr/bin/node

var fs = require('fs');
var path = require('path');

var fontsDir = './fonts/';
var cssFilename = './fonts.css'

var filenames = fs.readdirSync(fontsDir);
console.log('Converting '+filenames.length+' fonts to CSS...');

var cssWriteStream = fs.createWriteStream(cssFilename, { flags: 'w+' });

cssWriteStream.on('open', function(){
	filenames.forEach(function(filename){
		var fontFaceName = path.basename(filename, path.extname(filename));
		console.log(' '+fontFaceName);

		var style = weight = 'normal';

		if(/italic/i.test(fontFaceName)){
			style = 'italic';
			fontFaceName = fontFaceName.replace(/italic/i, '');
		}

		if(/bold/i.test(fontFaceName)){
			weight = 'bold';
			fontFaceName = fontFaceName.replace(/bold/i, '');
		}

		fontFaceName = fontFaceName.replace(/-$/, '');

		cssWriteStream.write("@font-face {\n"+
			"\tfont-family: '"+fontFaceName+"';\n"+
			"\tfont-weight: "+weight+";\n"+
			"\tfont-style: "+style+";\n"+
			"\tsrc: url(data:application/x-font-woff;charset=utf-8;base64,");
		cssWriteStream.write(fs.readFileSync(fontsDir + filename).toString('base64'));
		cssWriteStream.write(") format('woff');\n}\n\n");
	});
	cssWriteStream.end();

	cssWriteStream.on('close', function(){
		var filesizeBytes = fs.statSync(cssFilename).size;
		console.log('Saved '+path.basename(cssFilename) +" (" + (filesizeBytes/1024).toFixed(0) + " kbytes)");
	});
});
