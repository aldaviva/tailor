package vc.bjn.skinny.tailor.service.warehouse;

public abstract class QueryException extends Exception {

	private static final long serialVersionUID = 1L;

	private QueryException(final String message, final Throwable cause) {
		super(message, cause);
	}

	private QueryException(final String message) {
		super(message);
	}

	private QueryException(final Throwable cause){
		super(cause);
	}

	public static class MongoQueryException extends QueryException {

		private static final long serialVersionUID = 1L;

		public MongoQueryException(final Throwable t) {
			super(t);
		}

	}

}
