package vc.bjn.skinny.tailor.service.warehouse;

import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import vc.bjn.skinny.tailor.data.query.Query;
import vc.bjn.skinny.tailor.data.query.mongo.CountingQuery;
import vc.bjn.skinny.tailor.data.query.mongo.FeatureUsageQuery;
import vc.bjn.skinny.tailor.data.query.mongo.HistogramQuery;


@Configuration
public class QueryConfig {

	@Bean(name = "/attending/joinTime")
	public Query<Map<Double, Integer>> joinTime(){
		return new HistogramQuery("/attending/joinTime")
		.setFilterAction("interfaceEvents")
		.setFilterLabel("videoStarted");
	}

	@Bean(name = "/attending/joinCount")
	public Query<Integer> joinCount(){
		return new CountingQuery("/attending/joinCount")
			.setFilterAction("interfaceEvents")
			.setFilterLabel("videoStarted");
	}

	@Bean(name = "/attending/uiLoadCount")
	public Query<Integer> uiLoadCount(){
		return new CountingQuery("/attending/uiLoadCount")
			.setFilterAction("loadedInterface");
	}

	@Bean(name = "/installation/attempts")
	public Query<Integer> installationAttempts(){
		return new CountingQuery("/installation/attempts")
			.setFilterAction("downloadStarted");
	}

	@Bean(name = "/attending/uiLoadTime")
	public Query<Map<Double, Integer>> uiLoadTime(){
		return new HistogramQuery("/attending/uiLoadTime")
		.setFilterAction("loadedInterface");
	}

	@Bean(name = "/behavior/elementUsage")
	public Query<Map<String, Integer>> featureUsage(){
		return new FeatureUsageQuery("/behavior/elementUsage");
	}

	/*@Bean(name = "/attending/avgUiLoadTime")
	public Query<Average> pageLoadTime(){
		return new AveragingQuery("/attending/avgUiLoadTime")
			.setFilterAction("loadedInterface");
	}*/
}
