package vc.bjn.skinny.tailor.service;

import java.io.IOException;
import java.net.URI;

import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import vc.bjn.skinny.tailor.cache.Cacheable;

@Service
@Cacheable
public class LinesOfCodeServiceImpl implements LinesOfCodeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(LinesOfCodeServiceImpl.class);

	private @Value("${linesofcode.url.javascript}") URI javascriptUrl;
	private @Value("${linesofcode.url.css}") URI cssUrl;

	protected int getLinesOfCode(final URI uri) {
		try {
			final String responseStr = Request.Get(uri).execute().returnContent().asString();
			return Integer.parseInt(responseStr);
		} catch (IOException | NumberFormatException e) {
			LOGGER.error("Unable to get lines of code from "+uri, e);
			return 0;
		}
	}

	@Override
	public int getLinesOfJavascriptCode() {
		return getLinesOfCode(javascriptUrl);
	}

	@Override
	public int getLinesOfCssCode() {
		return getLinesOfCode(cssUrl);
	}

	/*
	 *  git clone http://git.bluejeansnet.com/repositories/skinny.git
	 *  cd skinny
	 *  for ext in *.c *.hpp *.h *.cpp *.cc; do echo -n "$ext: "; ( find ./ -name $ext -not -path '*party*' -not -path '*trunk/tools*' -print0 | xargs -0 cat ) | wc -l; done
	 */
	@Override
	public int getLinesOfCppCode() {
		return 611624;
	}

}
