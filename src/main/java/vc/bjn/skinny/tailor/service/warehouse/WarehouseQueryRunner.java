package vc.bjn.skinny.tailor.service.warehouse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vc.bjn.skinny.tailor.data.query.Query;

public class WarehouseQueryRunner<T> implements Runnable {

	private static final Logger LOGGER = LoggerFactory.getLogger(WarehouseQueryRunner.class);

	private final Query<T> query;

	public WarehouseQueryRunner(final Query<T> query) {
		this.query = query;
	}

	@Override
	public void run() {
		final String queryClassName = query.getClass().getSimpleName();
		LOGGER.debug("Running scheduled query {}", query.getResource());
		try {

			final T queryResult = query.query();
			LOGGER.trace("Query {} finished, caching result {} of type {}.", new Object[]{ query.getResource(), queryResult, queryResult.getClass().getSimpleName() });
			query.setCached(queryResult);

//			final T cached = query.getCached();
//			LOGGER.debug("Verifying write succeeded: read value {}", cached);
//			LOGGER.debug("After reading cached value, it has type {}", cached.getClass().getSimpleName());

		} catch (final QueryException e) {
			LOGGER.error("Error while running {}: {}", queryClassName, e.getMessage());
			e.printStackTrace();
		}
	}

}
