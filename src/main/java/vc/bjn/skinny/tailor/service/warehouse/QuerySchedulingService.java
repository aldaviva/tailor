package vc.bjn.skinny.tailor.service.warehouse;

import java.util.Iterator;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import vc.bjn.skinny.tailor.data.query.Query;

@Component
public class QuerySchedulingService {

	private static final Logger LOGGER = LoggerFactory.getLogger(QuerySchedulingService.class);

	private static final long QUERY_CYCLE_IN_MILLIS = Duration.standardMinutes(15).getMillis();

	@Autowired private Set<Query<?>> queries;

	private final TaskScheduler scheduler;

	public QuerySchedulingService(){
		scheduler = new ThreadPoolTaskScheduler();
		((ThreadPoolTaskScheduler) scheduler).initialize();
	}

	@PostConstruct
	public void scheduleQueriesByContext(){
		if(!queries.isEmpty()){
			final Duration taskStartDurationOffset = new Duration(QUERY_CYCLE_IN_MILLIS / queries.size());
			DateTime movingStartTime = new DateTime();

			final Iterator<Query<?>> queryIterator = queries.iterator();
			while (queryIterator.hasNext()) {
				final Query<?> query = queryIterator.next();

				final Runnable task = createQueryRunner(query);
				scheduler.scheduleAtFixedRate(task, movingStartTime.toDate(), QUERY_CYCLE_IN_MILLIS);
				LOGGER.trace("Scheduled {} to run every {} minutes, starting at {}.", new Object[]{ query.getResource(), Period.millis((int) QUERY_CYCLE_IN_MILLIS).toStandardMinutes().getMinutes(), movingStartTime });

				movingStartTime = movingStartTime.plus(taskStartDurationOffset);
			}

			LOGGER.debug("Scheduled {} queries.", queries.size());
		}
	}

	/*
	 * Generics! I'm so smart...
	 */
	private <T> Runnable createQueryRunner(final Query<T> query){
		return new WarehouseQueryRunner<>(query);
	}

}
