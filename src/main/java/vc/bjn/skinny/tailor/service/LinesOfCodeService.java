package vc.bjn.skinny.tailor.service;

public interface LinesOfCodeService {

	public int getLinesOfJavascriptCode();

	public int getLinesOfCssCode();

	public int getLinesOfCppCode();

}
