package vc.bjn.skinny.tailor.data.entity;

import java.beans.Transient;
import java.io.Serializable;

import org.joda.time.DateTime;
import org.joda.time.Days;

public class Release implements Serializable {

	private static final long serialVersionUID = 3683336479784316575L;

//	private String id;
//	private String revision;

	private String name;
	private DateTime codeFreezeDate;
	private DateTime releaseDate;

//	@JsonProperty("_id")
//	public String getId() {
//		return id;
//	}
//	public void setId(final String id) {
//		this.id = id;
//	}
//
//	@JsonProperty("_rev")
//	public String getRevision() {
//		return revision;
//	}
//	public void setRevision(final String revision) {
//		this.revision = revision;
//	}

	public String getName() {
		return name;
	}
	public void setName(final String name) {
		this.name = name;
	}

	public DateTime getCodeFreezeDate() {
		return codeFreezeDate;
	}
	public void setCodeFreezeDate(final DateTime codeFreezeDate) {
		this.codeFreezeDate = codeFreezeDate;
	}

	public DateTime getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(final DateTime releaseDate) {
		this.releaseDate = releaseDate;
	}

	@Transient
	public int getDaysUntilRelease(){
		return Days.daysBetween(new DateTime(), releaseDate).getDays();
	}

	@Transient
	public int getDaysUntilCodeFreeze(){
		return Days.daysBetween(new DateTime(), codeFreezeDate).getDays();
	}

}
