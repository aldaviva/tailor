package vc.bjn.skinny.tailor.data.provider;

import java.util.Map;

import vc.bjn.skinny.tailor.data.entity.Release;
import vc.bjn.skinny.tailor.data.entity.VersionMatrix;

public interface DevelopmentDataProvider {

	public static final String VERSION_MATRIX_ID = "versionMatrix";
	public static final String RELEASE_ID = "release";

	Release getRelease();

	VersionMatrix getVersionMatrix();

	void updateVersionMatrix(Map<String, Object> versionMatrix);

	void updateRelease(Map<String, Object> release);

}