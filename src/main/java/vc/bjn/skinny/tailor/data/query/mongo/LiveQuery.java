package vc.bjn.skinny.tailor.data.query.mongo;

import vc.bjn.skinny.tailor.async.Publisher;

public interface LiveQuery<T> {

	void publish(T value); //so mapped queries like feature usage can decide to do a full or partial publish

	void setPublisher(final Publisher publisher);

	T incr(T by);

}
