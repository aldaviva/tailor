package vc.bjn.skinny.tailor.data.query.mongo;

import java.util.Map;

import vc.bjn.skinny.tailor.data.entity.Average;
import vc.bjn.skinny.tailor.service.warehouse.QueryException;

import com.mongodb.DBObject;
import com.mongodb.MapReduceCommand;
import com.mongodb.MapReduceCommand.OutputType;
import com.mongodb.MapReduceOutput;
import com.mongodb.MongoException;


public class AveragingQuery extends LiveBaseQuery<Average> {

	public AveragingQuery(final String resource) {
		super(resource);
	}

	@Override
	public Average incr(final Average by) {
		final Average cached = getCached();
		cached.numerator += by.numerator;
		cached.denominator += by.denominator;
		publish(cached);
		return cached;
	}

	@Override
	public Average query() throws QueryException {
		try {
			final MapReduceCommand averageCommand = new MapReduceCommand(dbCollection, getMap(), getReduce(), null, OutputType.INLINE, filter);
			final MapReduceOutput averageResults = dbCollection.mapReduce(averageCommand);

			final DBObject resultDoc = averageResults.results().iterator().next();

			if(resultDoc != null){
				@SuppressWarnings("unchecked")
				final Map<String, Double> resultDocValue = (Map<String, Double>) resultDoc.get("value");

				final Average average = new Average();
				average.numerator = resultDocValue.get("numerator");
				average.denominator = resultDocValue.get("denominator");
				return average;
			} else {
				return null;
			}

		} catch (final MongoException e){
			throw new QueryException.MongoQueryException(e);
		}
	}

	private String getMap() {
		return "function(){" +
			"emit(1, { numerator: this.timesincestart, denominator: 1});" +
		"}";
	}

	private String getReduce() {
		return "function(key, values){" +
			"var numeratorSum = 0;" +
			"var denominatorSum = 0;" +
			"values.forEach(function(value){" +
				"numeratorSum += (value.numerator * value.denominator);" +
				"denominatorSum += value.denominator;" +
			"});" +
			"return { numerator: numeratorSum, denominator: denominatorSum };" +
		"}";
	}

}
