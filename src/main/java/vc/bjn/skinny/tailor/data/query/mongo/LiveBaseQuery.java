package vc.bjn.skinny.tailor.data.query.mongo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import vc.bjn.skinny.tailor.async.Publisher;

public abstract class LiveBaseQuery<T> extends CachedBaseQuery<T> implements LiveQuery<T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(LiveBaseQuery.class);

	protected Publisher publisher;

	public LiveBaseQuery(final String resource) {
		super(resource);
	}

	@Autowired
	@Override
	public void setPublisher(final Publisher publisher){
		this.publisher = publisher;
	}

	@Override
	public void publish(final T value) {
		LOGGER.trace("Live query {} updated its cached value to {}, publishing.", getResource(), value);
		publisher.publish(getLiveTopic(), value);
	}

	public final String getLiveTopic() {
		return "/events"+getResource();
	}

	@Override
	public void setCached(final T value) {
		super.setCached(value);
		publish(value);
	}
}