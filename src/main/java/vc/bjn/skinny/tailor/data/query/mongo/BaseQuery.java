package vc.bjn.skinny.tailor.data.query.mongo;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.springframework.beans.factory.annotation.Autowired;

import vc.bjn.skinny.tailor.data.ga.DateFilter;
import vc.bjn.skinny.tailor.data.query.Query;

import com.google.common.collect.ImmutableMap;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public abstract class BaseQuery<T> implements Query<T> {

	protected final String resource;
	protected DBCollection dbCollection;
	protected DBObject filter;

	public BaseQuery(final String resource) {
		this.resource = resource;
		filter = new BasicDBObject();
		setFilterInterval(null);
	}

	@Override
	public String getResource() {
		return resource;
	}

	@Autowired
	@Override
	public void setDBCollection(final DBCollection dbCollection) {
		this.dbCollection = dbCollection;
	}

	@Override
	public Query<T> setFilterInterval(final Interval eventInterval) {
		Interval eventIntervalOrDefault = eventInterval;
		if(eventIntervalOrDefault == null){
			eventIntervalOrDefault = new Interval(Days.days(DateFilter.DEFAULT_FILTER_DAYS), new DateTime());
		}
		filter.put("timestamp", ImmutableMap.of(
			"$gte", eventIntervalOrDefault.getStart().getMillis(),
			"$lte", eventIntervalOrDefault.getEnd().getMillis()));
		return this;
	}

	@Override
	public Query<T> setFilterAction(final String action){
		filter.put("action", action);
		return this;
	}

	@Override
	public Query<T> setFilterLabel(final String label){
		filter.put("label", label);
		return this;
	}

}