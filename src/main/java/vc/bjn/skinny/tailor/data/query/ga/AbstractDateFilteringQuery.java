package vc.bjn.skinny.tailor.data.query.ga;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import vc.bjn.skinny.tailor.api.util.RequestUriInfoHolder;
import vc.bjn.skinny.tailor.data.ga.DateFilter;

import com.google.api.services.analytics.Analytics;

public abstract class AbstractDateFilteringQuery {

	@Autowired protected Analytics analytics;
	@Autowired private RequestUriInfoHolder requestUriInfoHolder;

	private DateFilter dateFilter;

	@PostConstruct
	protected void init(){
		dateFilter = new DateFilter(requestUriInfoHolder);
	}

	protected final DateFilter getDateFilter(){
		if(dateFilter != null){
			return dateFilter;
		} else {
			throw new RuntimeException("Resource did not call AbstractDateFilteringQuery.setUriInfo() with the resource instance's UriInfo.");
		}
	}
}
