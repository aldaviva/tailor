package vc.bjn.skinny.tailor.data.couchbase;

import java.util.Map;

import org.springframework.stereotype.Component;

import vc.bjn.skinny.tailor.data.entity.Release;
import vc.bjn.skinny.tailor.data.entity.VersionMatrix;
import vc.bjn.skinny.tailor.data.provider.DevelopmentDataProvider;

@Component
public class DevelopmentDataProviderCouchbase extends CouchbaseDataProvider implements DevelopmentDataProvider {

	@Override
	public Release getRelease() {
		return getOrCreate(RELEASE_ID, Release.class);
	}

	@Override
	public void updateRelease(final Map<String, Object> updates) {
		update(RELEASE_ID, updates);
	}

	@Override
	public VersionMatrix getVersionMatrix() {
		return getOrCreate(VERSION_MATRIX_ID, VersionMatrix.class);
	}

	@Override
	public void updateVersionMatrix(final Map<String, Object> updates) {
		update(VERSION_MATRIX_ID, updates);
	}
}
