package vc.bjn.skinny.tailor.data.entity;

import java.io.Serializable;


public class VersionMatrix implements Serializable {

	private static final long serialVersionUID = 5999115988318615501L;

//	private String id;
//	private String revision;

	private String masterVersion;
	private String eVersion;
	private String stageVersion;
	private String prodVersion;

//	@JsonProperty("_id")
//	public String getId() {
//		return id;
//	}
//	public void setId(final String id) {
//		this.id = id;
//	}
//
//	@JsonProperty("_rev")
//	public String getRevision() {
//		return revision;
//	}
//	public void setRevision(final String revision) {
//		this.revision = revision;
//	}

	public String getMasterVersion() {
		return masterVersion;
	}
	public void setMasterVersion(final String masterVersion) {
		this.masterVersion = masterVersion;
	}

	public String geteVersion() {
		return eVersion;
	}
	public void seteVersion(final String eVersion) {
		this.eVersion = eVersion;
	}

	public String getStageVersion() {
		return stageVersion;
	}
	public void setStageVersion(final String stageVersion) {
		this.stageVersion = stageVersion;
	}

	public String getProdVersion() {
		return prodVersion;
	}
	public void setProdVersion(final String prodVersion) {
		this.prodVersion = prodVersion;
	}
}
