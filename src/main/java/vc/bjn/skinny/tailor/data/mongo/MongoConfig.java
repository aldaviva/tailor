package vc.bjn.skinny.tailor.data.mongo;

import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.mongodb.ServerAddress;

@Configuration
public class MongoConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(MongoConfig.class);

	private @Value("${mongo.host}") String host;
	private @Value("${mongo.port}") int port;
	private @Value("${mongo.database}") String database;
	private @Value("${mongo.collection}") String collectionName;

	@Bean(destroyMethod="close")
	public Mongo mongoConnection() throws MongoException, UnknownHostException{
		return new Mongo(new ServerAddress(host, port));
	}

	@Bean
	public DB mongoDatabase() throws UnknownHostException {
		return mongoConnection().getDB(database);
	}

	@Bean
	public DBCollection mongoEventsCollection() throws MongoException, UnknownHostException {
		final DBCollection collection = mongoDatabase().getCollection(collectionName);
		LOGGER.info("MongoDB connected.");
		return collection;
	}

}
