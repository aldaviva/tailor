package vc.bjn.skinny.tailor.data.couchbase;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.couchbase.client.CouchbaseClient;

public abstract class CouchbaseDataProvider {

	private static final Logger LOGGER = LoggerFactory.getLogger(CouchbaseDataProvider.class);

	@Autowired protected CouchbaseClient couchbase;

	protected final <T> T getOrCreate(final String key, final Class<T> type){
		@SuppressWarnings("unchecked")
		final T entity = (T) couchbase.get(key);

		if(entity != null){
			return entity;
		} else {
			try {
				final T newInstance = type.newInstance();
				couchbase.set(key, 0, newInstance);
				return newInstance;
			} catch (InstantiationException | IllegalAccessException e) {
				LOGGER.error("Unable to create new entity {}: {}", type.getName(), e.getMessage());
				throw new RuntimeException(e);
			}
		}
	}

	protected final <T> void update(final String key, final Map<String, Object> updates){
		final Object existingObj = couchbase.get(key);

		try {
			//BeanUtils.populate(existingObj, updates);

			for (final Entry<String, Object> update : updates.entrySet()) {
				LOGGER.trace("Updating field {} to {}.", update.getKey(), update.getValue());
				BeanUtils.copyProperty(existingObj, update.getKey(), update.getValue());
			}
		} catch (IllegalAccessException | InvocationTargetException e) {
			LOGGER.error("Unable to update entity {}: {}", key, e.getMessage());
			throw new RuntimeException(e);
		}

		couchbase.set(key, 0, existingObj);
	}
}
