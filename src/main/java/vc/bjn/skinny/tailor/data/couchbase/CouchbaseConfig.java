package vc.bjn.skinny.tailor.data.couchbase;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.CouchbaseConnectionFactory;
import com.couchbase.client.CouchbaseConnectionFactoryBuilder;

@Configuration
public class CouchbaseConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(CouchbaseConfig.class);

	private @Value("${couchbase.host}") String host;
	private @Value("${couchbase.port}") int port;
	private @Value("${couchbase.bucket}") String bucket;
	private @Value("${couchbase.password}") String password;

	@Bean(destroyMethod="shutdown")
	public CouchbaseClient couchbaseClient() throws IOException, URISyntaxException{
		final List<URI> uris = Collections.singletonList(new URI("http", null, host, port, "/pools", null, null));
		final CouchbaseConnectionFactory connectionFactory = new CouchbaseConnectionFactoryBuilder()
			.buildCouchbaseConnection(uris, bucket, password);

		final CouchbaseClient couchbaseClient = new CouchbaseClient(connectionFactory);

		LOGGER.info("Couchbase connected.");
		return couchbaseClient;
	}

}
