package vc.bjn.skinny.tailor.data.query;

public interface QueryLocator {

	public Query<?> getQuery(String resource);

}
