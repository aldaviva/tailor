package vc.bjn.skinny.tailor.data.query;

import org.joda.time.Interval;

import vc.bjn.skinny.tailor.service.warehouse.QueryException;

import com.mongodb.DBCollection;

public interface Query<T> {

	void setDBCollection(DBCollection dbCollection);

	T query() throws QueryException;

	T getCached();

	void setCached(T value);

	String getResource();

	Query<T> setFilterInterval(Interval eventInterval);

	Query<T> setFilterAction(String action);

	Query<T> setFilterLabel(String label);

}
