package vc.bjn.skinny.tailor.data.entity;

import org.codehaus.jackson.annotate.JsonProperty;

@Deprecated
public class Codebase {

	private String id;
	private String revision;

	private int cppLines;
	private int jsLines;
	private int cssLines;

	@JsonProperty("_id")
	public String getId() {
		return id;
	}
	public void setId(final String id) {
		this.id = id;
	}

	@JsonProperty("_rev")
	public String getRevision() {
		return revision;
	}
	public void setRevision(final String revision) {
		this.revision = revision;
	}

	public int getCppLines() {
		return cppLines;
	}
	public void setCppLines(final int cppLines) {
		this.cppLines = cppLines;
	}

	public int getJsLines() {
		return jsLines;
	}
	public void setJsLines(final int jsLines) {
		this.jsLines = jsLines;
	}

	public int getCssLines() {
		return cssLines;
	}
	public void setCssLines(final int cssLines) {
		this.cssLines = cssLines;
	}



}
