package vc.bjn.skinny.tailor.data.ga;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.analytics.Analytics;

@Configuration
@SuppressWarnings("unused")
public class AnalyticsConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(AnalyticsConfig.class);

	@Bean
	public Analytics googleAnalytics(){
		final String CLIENT_ID = "621061747354-ai4s3esp0u9ibthju7ajs3bl19c9mqll.apps.googleusercontent.com";
		final String CLIENT_SECRET = "6glOwmfnw4QimBblQ2MDOh9_";
		final String REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";

		final JacksonFactory jacksonFactory = new JacksonFactory();
		final NetHttpTransport netHttpTransport = new NetHttpTransport();

//		final String authUrl = new GoogleAuthorizationCodeRequestUrl(CLIENT_ID, REDIRECT_URI, Arrays.asList(AnalyticsScopes.ANALYTICS_READONLY)).build();
//		System.out.println("Authorization URL: "+authUrl);


		final String authCode = "4/JautMT0gyeUAFQLwYDaIhbnJJV97.kglZvYCPh8gQuJJVnL49Cc_SaX6VcAI";
		final String refreshToken = "1/ifrrY74kwwuKwg6qVifQg4gY9NRMTgtNLhCgSxbMtVI";

		/*final GoogleAuthorizationCodeTokenRequest tokenRequest = new GoogleAuthorizationCodeTokenRequest(netHttpTransport, jacksonFactory, CLIENT_ID, CLIENT_SECRET, authCode, REDIRECT_URI);
//			.setGrantType("refresh_token");
		final GoogleTokenResponse tokenResponse = tokenRequest.execute();
		final String refreshToken = tokenResponse.getRefreshToken();
		System.out.println("Refresh token:" + refreshToken);*/

		final GoogleCredential credential = new GoogleCredential.Builder()
			.setJsonFactory(jacksonFactory)
			.setTransport(netHttpTransport)
			.setClientSecrets(CLIENT_ID, CLIENT_SECRET).build();
		credential.setRefreshToken(refreshToken);

		return new Analytics.Builder(netHttpTransport, jacksonFactory, credential)
			.setApplicationName("vc.bjn.skinny.tailor")
			.build();
	}

}
