package vc.bjn.skinny.tailor.data.marshal;

import javax.annotation.PostConstruct;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class JodaBeanUtilsConverter {

	private static final Logger LOGGER = LoggerFactory.getLogger(JodaBeanUtilsConverter.class);

	@PostConstruct
	public void registerConverter(){
		LOGGER.trace("Registering BeanUtils converter for Joda DateTime");

		ConvertUtils.register(new Converter() {

			@Override
			public Object convert(@SuppressWarnings("rawtypes") final Class type, final Object value) {
				return new DateTime(value);
			}
		}, DateTime.class);
	}

}
