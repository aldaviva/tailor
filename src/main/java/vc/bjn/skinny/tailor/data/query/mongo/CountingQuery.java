package vc.bjn.skinny.tailor.data.query.mongo;

import vc.bjn.skinny.tailor.service.warehouse.QueryException;

public class CountingQuery extends LiveBaseQuery<Integer> {

	public CountingQuery(final String resource) {
		super(resource);
	}

	@Override
	public Integer query() throws QueryException {
		return (int) dbCollection.count(filter);
	}

	/**
	 * @param value
	 */
	@Override
	public Integer incr(final Integer by) {
		final int newValue = cache.incr(getCacheKey(), by);
		publish(newValue);
		return newValue;
	}

	@Override
	public void setCached(final Integer value) {
		cache.set(getCacheKey(), value.toString()); //to allow incr/decr
		publish(value);
	}

	@Override
	public Integer getCached() {
		final String valueAsString = cache.get(getCacheKey());
		return Integer.valueOf(valueAsString);
	}

}
