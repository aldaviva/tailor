package vc.bjn.skinny.tailor.data.query.mongo;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import vc.bjn.skinny.tailor.service.warehouse.QueryException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MapReduceCommand;
import com.mongodb.MapReduceOutput;
import com.mongodb.MongoException;

public class FeatureUsageQuery extends LiveBaseQuery<Map<String, Integer>> {

	private static final String TEMP_PHASE1_RESULTS_COLLECTION = "tempFeatureUsagePhase1";

	public FeatureUsageQuery(final String resource) {
		super(resource);
		setFilterAction("elements");
	}

	@Override
	public Map<String, Integer> query() throws QueryException {
		try {
			final MapReduceCommand eventsPerSessionAndElement = new MapReduceCommand(dbCollection, getMap1Script(), getReduceScript(), TEMP_PHASE1_RESULTS_COLLECTION, MapReduceCommand.OutputType.REPLACE, filter);
			final MapReduceOutput phase1Results = dbCollection.mapReduce(eventsPerSessionAndElement);

			final DBCollection phase1ResultsCollection = phase1Results.getOutputCollection();

			final MapReduceCommand sessionsPerEvent = new MapReduceCommand(phase1ResultsCollection, getMap2Script(), getReduceScript(), null, MapReduceCommand.OutputType.INLINE, null);
			final MapReduceOutput phase2Results = phase1ResultsCollection.mapReduce(sessionsPerEvent);

			final Map<String, Integer> resultMap = new HashMap<>();
			for(final DBObject resultRow : phase2Results.results()) {
				final String element = (String) resultRow.get("_id");
				final Integer count = ((Double) resultRow.get("value")).intValue();
				resultMap.put(element, count);
			}

			phase1ResultsCollection.remove(new BasicDBObject());

			return resultMap;

		} catch (final MongoException e){
			throw new QueryException.MongoQueryException(e);
		}
	}

	@Override
	public Map<String, Integer> incr(final Map<String, Integer> by) {
		final Map<String, Integer> cachedMap = getCached();
		final Map<String, Integer> mapWithChangedValuesOnly = new HashMap<>();
		for (final Entry<String, Integer> byEntry : by.entrySet()) {
			final String key = byEntry.getKey();
			Integer cached = cachedMap.get(key);
			if(cached == null){
				cached = 0;
			}
			cached += byEntry.getValue();
			cachedMap.put(key, cached); //TODO not synchronized, might lose updates
			mapWithChangedValuesOnly.put(key, cached);
			publisher.publish(getLiveTopic()+"/"+key, cached);
		}
		cache.set(getCacheKey(), cachedMap);
		return mapWithChangedValuesOnly;
	}

	private String getMap1Script(){
		return "function(){"+
			"emit({ epguid: this.epguid, label: this.label }, 1);"+
		 "}";
	}

	private String getReduceScript(){
		return "function(key, values){" +
			"var count = 0;" +
			"values.forEach(function(value){" +
				"count += value;" +
			"});"+
			"return count;" +
		"}";
	}

	private String getMap2Script(){
		return "function(){"+
			"emit(this._id.label, 1);"+
		"}";
	}

	/**
	 * Here is the mongo shell script that does what I want:
	 *
	 * var temp_collection = "TEMP";
	 *
	 * function map1(){
	 *     emit({epguid: this.epguid, label: this.label}, {count: 1});
	 * }
	 *
	 * function reduce1(key, values){
	 *     var count = 0;
	 *     values.forEach(function(value){
	 *         count += value.count;
	 *     });
	 *     return { count: count };
	 * }
	 *
	 * function map2(){
	 *     emit({ label: this._id.label }, { count: 1});
	 * }
	 *
	 * var eventsPerEpguidAndElement = db.events.mapReduce(map1, reduce1, { out: { replace: temp_collection }, query: { "a\
	 * ction": "elements" }});
	 *
	 * var countSessionsWithAtLeastOneElementByElement = db[temp_collection].mapReduce(map2, reduce1, { out: { inline: 1} \
	 * });
	 *
	 * db[temp_collection].remove();
	 *
	 * countSessionsWithAtLeastOneElementByElement.results.forEach(printjson);
	 */
}
