package vc.bjn.skinny.tailor.data.entity;

import java.io.Serializable;

public class Average implements Serializable {

	private static final long serialVersionUID = -5663650436899536466L;

	public double numerator;
	public double denominator;

	public Average(){
		numerator = 0;
		denominator = 0;
	}

	public Average(final double numerator, final double denominator) {
		this.numerator = numerator;
		this.denominator = denominator;
	}

	public Double getAverage(){
		if(denominator != 0){
			return numerator / denominator;
		} else {
			return null;
		}
	}
}
