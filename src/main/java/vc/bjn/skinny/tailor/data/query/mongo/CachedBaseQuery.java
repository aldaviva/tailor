package vc.bjn.skinny.tailor.data.query.mongo;

import org.springframework.beans.factory.annotation.Autowired;

import vc.bjn.skinny.tailor.cache.Cache;

public abstract class CachedBaseQuery<T> extends BaseQuery<T> {

	@Autowired
	protected Cache cache;

	public CachedBaseQuery(final String resource){
		super(resource);
	}

	@Override
	public T getCached() {
		return cache.get(getCacheKey());
	}

	@Override
	public void setCached(final T value) {
		cache.set(getCacheKey(), value);
	}

	public final String getCacheKey() {
		return Cache.WAREHOUSE_PREFIX+getResource();
	}

}
