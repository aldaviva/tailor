package vc.bjn.skinny.tailor.data.ga;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

import vc.bjn.skinny.tailor.api.util.RequestUriInfoHolder;

public class DateFilter {

	private final DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	public static final int DEFAULT_FILTER_DAYS = 28;

	private Integer _days;
	private final RequestUriInfoHolder requestUriInfoHolder;

	public DateFilter(final RequestUriInfoHolder requestUriInfoHolder){
		this.requestUriInfoHolder = requestUriInfoHolder;
	}

	private int getDays(){
		if(_days == null){
			final MultivaluedMap<String, String> queryParameters = requestUriInfoHolder.getUriInfo().getQueryParameters();
			final List<String> daysParam = queryParameters.get("days");
			_days = (daysParam != null) ? Integer.parseInt(daysParam.get(0)) : DEFAULT_FILTER_DAYS;
		}

		return _days;
	}

	public Date getFilterStartDate(){
		final Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1 * getDays());
		return calendar.getTime();
	}

	public String getFilterStartDateAsString(){
		return dateFormatter.format(getFilterStartDate());
	}

	public Date getFilterEndDate(){
		return new Date();
	}

	public String getFilterEndDateAsString(){
		return dateFormatter.format(getFilterEndDate());
	}
}
