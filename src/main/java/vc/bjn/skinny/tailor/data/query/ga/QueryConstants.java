package vc.bjn.skinny.tailor.data.query.ga;

public interface QueryConstants {

	public static final String SEGMENT_SKYY = "gaid::950842805";
	public static final String SEGMENT_ALL = "gaid::-1";
	public static final String SEGMENT = SEGMENT_ALL;

	public static final String PROFILE_WWW = "ga:38370386";
	public static final String PROFILE_ALL = "ga:53601410";
	public static final String PROFILE = PROFILE_WWW;

}
