package vc.bjn.skinny.tailor.data.query.mongo;

import java.util.HashMap;
import java.util.Map;

import vc.bjn.skinny.tailor.service.warehouse.QueryException;

import com.mongodb.DBObject;
import com.mongodb.MapReduceCommand;
import com.mongodb.MapReduceOutput;
import com.mongodb.MongoException;

public class HistogramQuery extends CachedBaseQuery<Map<Double, Integer>> {

	private static final double DIVISOR = 1000.0;

	public HistogramQuery(final String resource) {
		super(resource);
	}

	@Override
	public Map<Double, Integer> query() throws QueryException {
		try {
			final MapReduceCommand jobSpec = new MapReduceCommand(dbCollection, getMapScript(), getReduceScript(), null, MapReduceCommand.OutputType.INLINE, filter);
			final MapReduceOutput jobResults = dbCollection.mapReduce(jobSpec);

			double minBucket = Double.POSITIVE_INFINITY;
			double maxBucket = Double.NEGATIVE_INFINITY;

			final Map<Double, Integer> resultMap = new HashMap<>();
			for(final DBObject resultDoc : jobResults.results()) {
				final Double bucket = (Double) resultDoc.get("_id");
				final Integer count = ((Double) resultDoc.get("value")).intValue();
				resultMap.put(bucket, count);

				minBucket = Math.min(minBucket, bucket);
				maxBucket = Math.max(maxBucket, bucket);
			}

			for(double bucket=minBucket; bucket < maxBucket; bucket += DIVISOR){
				if(!resultMap.containsKey(bucket)){
					resultMap.put(bucket, 0);
				}
			}

			return resultMap;

		} catch (final MongoException e){
			throw new QueryException.MongoQueryException(e);
		}
	}

	private String getMapScript(){
		return "function(){" +
			"emit(Math.floor(this.timesincestart/"+DIVISOR+")*"+DIVISOR+", 1);" +
		"}";
	}

	private String getReduceScript(){
		return "function(bucket, values){" +
			"var count = 0;" +
			"values.forEach(function(value){" +
				"count += value;" +
			"});"+
			"return count;" +
		"}";
	}
}
