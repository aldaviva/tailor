package vc.bjn.skinny.tailor.data.query.ga;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import vc.bjn.skinny.tailor.cache.Cacheable;

import com.google.api.services.analytics.model.GaData;

@Component
@Scope("prototype") //is this necessary?
@Cacheable
public class InstallationQuery extends AbstractDateFilteringQuery {

	private static final Logger LOGGER = LoggerFactory.getLogger(InstallationQuery.class);
	private static final List<String> INSTALLATION_METHODS = Arrays.asList("manual", "autoInstall", "autoUpdate");

	private static final String KEY_FAILURE = "failure";
	private static final String KEY_SUCCESS = "success";

	private final DateFormat resultDateFormat = new SimpleDateFormat("yyyyMMdd");

	public Map<String, Integer> getSuccessAndFailureCount() throws IOException{
		final GaData remoteResponse = analytics.data().ga()
				.get(QueryConstants.PROFILE,
					getDateFilter().getFilterStartDateAsString(),
					getDateFilter().getFilterEndDateAsString(),
					"ga:totalEvents")
				.setDimensions("ga:eventLabel,ga:eventAction,ga:eventCategory")
				.setFilters("ga:eventCategory=~inmeeting.*")
				.setSegment(QueryConstants.SEGMENT)
				.execute();

		final Map<String, Integer> result = new HashMap<>();
		final List<List<String>> rows = remoteResponse.getRows();

		for (final List<String> row : rows) {
			if(row.get(1).equals("pluginInstallStatus") && row.get(2).equals("inmeeting-installation")){
				final Integer value = Integer.valueOf(row.get(3));
				final String label = row.get(0);
				result.put(label, value);
			}
		}

		return result;
	}

	public Map<Date, Map<String, Integer>> getVersionsInstalledOverTime() throws IOException{
		final GaData remoteResponse = analytics.data().ga()
			.get(QueryConstants.PROFILE,
				getDateFilter().getFilterStartDateAsString(),
				getDateFilter().getFilterEndDateAsString(),
				"ga:totalEvents")
			.setDimensions("ga:date,ga:eventLabel")
			.setFilters("ga:eventCategory==inmeeting-installation;ga:eventAction==downloadStarted")
			.setSegment(QueryConstants.SEGMENT)
			.execute();

		final List<List<String>> rows = remoteResponse.getRows();
		final Map<Date, Map<String, Integer>> result = new HashMap<>();

		for (final List<String> row : rows) {
			try {
				final Date date = resultDateFormat.parse(row.get(0));
				final String version = row.get(1);
				final int count = Integer.parseInt(row.get(2));

				Map<String, Integer> resultBucket = result.get(date);
				if(resultBucket == null){
					resultBucket = new HashMap<>();
					result.put(date, resultBucket);
				}

				resultBucket.put(version, count);

			} catch (final ParseException e) {
				LOGGER.info("getVersionsInstalledOverTime: Skipped row with unparsable date {}", row.get(0));
			}
		}

		return result;
	}

	public Integer getDownloadAttempts() throws IOException {
		final GaData remoteResponse = analytics.data().ga()
			.get(QueryConstants.PROFILE,
				getDateFilter().getFilterStartDateAsString(),
				getDateFilter().getFilterEndDateAsString(),
				"ga:totalEvents")
			.setFilters("ga:eventCategory==inmeeting-installation;ga:eventAction==downloadStarted")
			.setSegment(QueryConstants.SEGMENT)
			.execute();

		return Integer.valueOf(remoteResponse.getRows().get(0).get(0));
	}

	public Map<String, Map<String, Number>> getManualTime() throws IOException {
		final GaData remoteResponse = analytics.data().ga()
			.get(QueryConstants.PROFILE,
				getDateFilter().getFilterStartDateAsString(),
				getDateFilter().getFilterEndDateAsString(),
				"ga:avgEventValue,ga:totalEvents")
			.setDimensions("ga:operatingSystem")
			.setFilters("ga:eventCategory==inmeeting-installation;ga:eventLabel==success")
			.setSegment(QueryConstants.SEGMENT)
			.execute();

		final Map<String, Map<String, Number>> result = new HashMap<>();

		final List<List<String>> rows = remoteResponse.getRows();

		for (final List<String> row : rows) {
			final Map<String, Number> osEntry = new HashMap<>();
			final String key = row.get(0).substring(0, 3).toLowerCase();
			result.put(key, osEntry);

			final double avgTime = Double.parseDouble(row.get(1));
			osEntry.put("avgTime", avgTime);

			final int count = Integer.parseInt(row.get(2));
			osEntry.put("count", count);
		}

		return result;
	}

	public Map<String, Map<String, Integer>> getAttemptsByMethod() throws IOException {
		final GaData remoteResponse = analytics.data().ga()
			.get(QueryConstants.PROFILE,
				getDateFilter().getFilterStartDateAsString(),
				getDateFilter().getFilterEndDateAsString(),
				"ga:totalEvents")
			.setDimensions("ga:eventAction,ga:eventLabel")
			.setFilters("ga:eventCategory==inmeeting-installation;ga:eventAction=~^plugin")
			.setSegment(QueryConstants.SEGMENT)
			.execute();

		final List<List<String>> rows = remoteResponse.getRows();
		final Map<String, Map<String, Integer>> result = new HashMap<String, Map<String, Integer>>();

		for (final String installationMethod : INSTALLATION_METHODS) {
			final Map<String, Integer> successAndFailureCount = new HashMap<>(2);
			successAndFailureCount.put(KEY_SUCCESS, 0);
			successAndFailureCount.put(KEY_FAILURE, 0);
			result.put(installationMethod, successAndFailureCount);
		}

		for (final List<String> row : rows) {
			final String isSuccess = row.get(1).equals(KEY_FAILURE) ? KEY_FAILURE : KEY_SUCCESS;

			String method;
			switch(row.get(0)){
			case "pluginInstallStatus":
				method = "manual";
				break;
			case "pluginAutoInstallStatus":
				method = "autoInstall";
				break;
			case "pluginAutoUpdateStatus":
				method = "autoUpdate";
				break;
			default:
				continue;
			}

			final Integer value = Integer.valueOf(row.get(2));
			result.get(method).put(isSuccess, value);
		}

		return result;
	}

	public Map<String, Double> getTimeByUpdateMethod() throws IOException {
		final GaData remoteResponse = analytics.data().ga()
			.get(QueryConstants.PROFILE,
				getDateFilter().getFilterStartDateAsString(),
				getDateFilter().getFilterEndDateAsString(),
				"ga:avgEventValue")
			.setDimensions("ga:eventAction")
			.setFilters("ga:eventCategory==inmeeting-installation;ga:eventAction=~^plugin;ga:eventLabel!=failure")
			.setSegment(QueryConstants.SEGMENT)
			.execute();

		final List<List<String>> rows = remoteResponse.getRows();
		final Map<String, Double> result = new HashMap<>();
		result.put("manual", 0.0);
		result.put("autoInstall", 0.0);
		result.put("autoUpdate", 0.0);

		for (final List<String> row : rows) {
			String key;
			switch(row.get(0)){
			case "pluginInstallStatus":
				key = "manual";
				break;
			case "pluginAutoInstallStatus":
				key = "autoInstall";
				break;
			case "pluginAutoUpdateStatus":
				key = "autoUpdate";
				break;
			default:
				continue;
			}
			final Double value = Double.valueOf(row.get(1));
			result.put(key, value);
		}

		return result;
	}
}
