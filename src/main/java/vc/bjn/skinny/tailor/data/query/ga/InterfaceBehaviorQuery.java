package vc.bjn.skinny.tailor.data.query.ga;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import vc.bjn.skinny.tailor.cache.Cacheable;

import com.google.api.services.analytics.model.GaData;

@Component
@Scope("prototype")
@Cacheable
public class InterfaceBehaviorQuery extends AbstractDateFilteringQuery {

	public Map<String, Integer> getSuccessAndFailureCount() throws IOException{
		final GaData remoteResponse = analytics.data().ga()
			.get(QueryConstants.PROFILE,
				getDateFilter().getFilterStartDateAsString(),
				getDateFilter().getFilterEndDateAsString(),
				"ga:totalEvents")
			.setDimensions("ga:eventLabel,ga:eventAction,ga:eventCategory")
			.setFilters("ga:eventCategory=~inmeeting.*")
			.setSegment(QueryConstants.SEGMENT)
			.execute();

		final Map<String, Integer> result = new HashMap<>();
		final List<List<String>> rows = remoteResponse.getRows();

		for (final List<String> row : rows) {
			if(row.get(1).equals("pluginInstallStatus") && row.get(2).equals("inmeeting-installation")){
				final Integer value = Integer.valueOf(row.get(3));
				final String label = row.get(0);
				result.put(label, value);
			}
		}

		return result;
	}

	public Double getAverageUILoadTime() throws IOException {
		final GaData remoteResponse = analytics.data().ga()
			.get(QueryConstants.PROFILE,
				getDateFilter().getFilterStartDateAsString(),
				getDateFilter().getFilterEndDateAsString(),
				"ga:totalEvents,ga:avgEventValue")
			.setDimensions("ga:hostname") //needed for filter below, otherwise 0 rows are returned
			.setFilters("ga:eventCategory==inmeeting-behaviors,ga:eventAction==loadedInterface;ga:eventValue<1000000") //some dev environments return large values because they don't publish the events correctly
			.setSegment(QueryConstants.SEGMENT)
			.execute();

		final List<List<String>> rows = remoteResponse.getRows();

		int totalEvents = 0;
		double averageNumerator = 0;

		for (final List<String> row : rows) {
			final int count = Integer.parseInt(row.get(1));
			totalEvents += count;
			averageNumerator += (totalEvents * Double.parseDouble(row.get(2)));
		}

		if(totalEvents > 0){
			return averageNumerator / totalEvents;
		} else {
			return 0.0;
		}
	}

	public Integer getUILoadCount() throws IOException {
		final GaData remoteResponse = analytics.data().ga()
			.get(QueryConstants.PROFILE,
				getDateFilter().getFilterStartDateAsString(),
				getDateFilter().getFilterEndDateAsString(),
				"ga:totalEvents")
			.setFilters("ga:eventCategory==inmeeting-behaviors,ga:eventAction==loadedInterface")
			.setSegment(QueryConstants.SEGMENT)
			.execute();

		return Integer.valueOf(remoteResponse.getRows().get(0).get(0));
	}

	public Double getAverageJoinTime() throws IOException {
		final GaData remoteResponse = analytics.data().ga()
			.get(QueryConstants.PROFILE,
				getDateFilter().getFilterStartDateAsString(),
				getDateFilter().getFilterEndDateAsString(),
				"ga:avgEventValue")
			.setFilters("ga:eventCategory==inmeeting-behaviors,ga:eventAction==interfaceEvents,ga:eventLabel==videoStarted")
			.setSegment(QueryConstants.SEGMENT)
			.execute();

		return Double.valueOf(remoteResponse.getRows().get(0).get(0));
	}

	public Integer getJoinCount() throws IOException {
		final GaData remoteResponse = analytics.data().ga()
			.get(QueryConstants.PROFILE,
				getDateFilter().getFilterStartDateAsString(),
				getDateFilter().getFilterEndDateAsString(),
				"ga:totalEvents")
			.setFilters("ga:eventCategory==inmeeting-behaviors,ga:eventAction==interfaceEvents,ga:eventLabel==videoStarted")
			.setSegment(QueryConstants.SEGMENT)
			.execute();

		return Integer.valueOf(remoteResponse.getRows().get(0).get(0));
	}


}
