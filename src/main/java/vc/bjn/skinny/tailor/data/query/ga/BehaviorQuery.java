package vc.bjn.skinny.tailor.data.query.ga;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import vc.bjn.skinny.tailor.cache.Cacheable;

import com.google.api.services.analytics.model.GaData;

@Component
@Scope("prototype")
@Cacheable
public class BehaviorQuery extends AbstractDateFilteringQuery {

	public Map<String, Integer> getElementUsage() throws IOException {
		final GaData remoteResponse = analytics.data().ga()
			.get(QueryConstants.PROFILE,
					getDateFilter().getFilterStartDateAsString(),
					getDateFilter().getFilterEndDateAsString(),
					"ga:totalEvents")
			.setDimensions("ga:eventLabel")
			.setFilters("ga:eventCategory==inmeeting-behaviors;ga:eventAction==elements")
			.setSegment(QueryConstants.SEGMENT)
			.execute();

		final List<List<String>> rows = remoteResponse.getRows();

		final Map<String, Integer> result = new HashMap<>();

		for (final List<String> row : rows) {
			final String key = row.get(0);
			final Integer value = Integer.valueOf(row.get(1));
			result.put(key, value);
		}

		return result;
	}

	public Map<String, Integer> getModerationRatio() throws IOException {
		return getRatio("isModerator", "moderator", "non-moderator");
	}

	public Map<String, Integer> getAuthenticatedRatio() throws IOException {
		return getRatio("authenticated", "authenticated", "anonymous");
	}

	Map<String, Integer> getRatio(final String eventAction, final String trueKey, final String falseKey) throws IOException {
		final GaData remoteResponse = analytics.data().ga()
			.get(QueryConstants.PROFILE,
					getDateFilter().getFilterStartDateAsString(),
					getDateFilter().getFilterEndDateAsString(),
					"ga:visits")
			.setDimensions("ga:eventLabel")
			.setFilters("ga:eventCategory==inmeeting-platform;ga:eventAction=="+eventAction)
			.setSegment(QueryConstants.SEGMENT)
			.execute();

		final List<List<String>> rows = remoteResponse.getRows();

		final Map<String, Integer> result = new HashMap<>();

		for (final List<String> row : rows) {
			final String key = (Boolean.parseBoolean(row.get(0))) ? trueKey : falseKey;
			final int count = Integer.valueOf(row.get(1));
			result.put(key, count);
		}

		return result;
	}

}
