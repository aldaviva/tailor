package vc.bjn.skinny.tailor.data.query.mongo;

import vc.bjn.skinny.tailor.data.query.WarehouseQuery;

public interface CachedQuery<T> extends WarehouseQuery<T> {

	@Override
	T getCached();

}