package vc.bjn.skinny.tailor.data.query.ga;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import vc.bjn.skinny.tailor.cache.Cacheable;

import com.google.api.services.analytics.model.GaData;

@Component
@Scope("prototype")
@Cacheable
public class PlatformShareQuery extends AbstractDateFilteringQuery {

	@Autowired private BehaviorQuery behaviorQuery;

	public Map<String, Integer> getBrowserShare() throws IOException {
		final GaData remoteResponse = analytics.data().ga()
				.get(QueryConstants.PROFILE,
						getDateFilter().getFilterStartDateAsString(),
						getDateFilter().getFilterEndDateAsString(),
						"ga:visits")
				.setDimensions("ga:browser")
				.setSegment(QueryConstants.SEGMENT)
				.execute();

		final Map<String, Integer> result = new HashMap<>();

		final List<List<String>> responseRows = remoteResponse.getRows();
		for (final List<String> row : responseRows) {
			final Integer visitCount = Integer.valueOf(row.get(1));

			String browserName = row.get(0);
			switch(browserName){
				case "Internet Explorer":
				case "IE with Chrome Frame":
					browserName = "IE";
					break;
				case "Chrome":
				case "Android Browser":
					browserName = "Chrome";
					break;
				case "Safari":
				case "Mozilla Compatible Agent":
					browserName = "Safari";
					break;
				case "Firefox":
					break;
				case "Opera": //sorry opera
				default:
					browserName = "other";
					break;
			}
			browserName = browserName.toLowerCase();

			final Integer oldVisitCount = result.get(browserName);
			result.put(browserName, visitCount + (oldVisitCount != null ? oldVisitCount : 0));
		}

		return result;
	}

	public Map<String, Integer> getOsShare() throws IOException {
		final GaData remoteResponse = analytics.data().ga()
			.get(QueryConstants.PROFILE,
					getDateFilter().getFilterStartDateAsString(),
					getDateFilter().getFilterEndDateAsString(),
					"ga:visits")
			.setDimensions("ga:operatingSystem,ga:operatingSystemVersion")
			.setFilters("ga:operatingSystem==Windows,ga:operatingSystem==Macintosh")
			.setSegment(QueryConstants.SEGMENT)
			.execute();

		final Map<String, Integer> result = new HashMap<>();

		final List<List<String>> responseRows = remoteResponse.getRows();
		for (final List<String> row : responseRows) {
			final String osName = row.get(0);
			final String osVersion = row.get(1);
			final Integer visitCount = Integer.valueOf(row.get(2));

			String osKey;
			if(osName.equals("Windows")){
				switch(osVersion){
					case "CE":
					case "(not set)":
					case "2000":
					case "NT":
						osKey = "other";
						break;
					case "Server 2003":
						osKey = "XP";
						break;
					default:
						osKey = osVersion;
				}
			} else { //Macintosh, by the analytics filter
				switch(osVersion){
					case "(not set)":
					case "PPC":
					case "Intel":
						osKey = "other";
						break;
					default:
						osKey = osVersion.replace("Intel ", "").replace("PPC ", "");
				}
			}
			if(!osKey.equals("other")){
				osKey = (osName.substring(0, 3) + " " + osKey.trim()).toLowerCase();
			}

			final Integer oldVisitCount = result.get(osKey);
			result.put(osKey, visitCount + (oldVisitCount != null ? oldVisitCount : 0));
		}

		return result;
	}

	public Map<String, Integer> getPlatformSupportedAndUnsupportedCount() throws IOException {
		return behaviorQuery.getRatio("supported", "supported", "unsupported");
	}

}
