package vc.bjn.skinny.tailor.api.marshal;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Component;

import vc.bjn.skinny.tailor.data.entity.Average;

import com.sun.jersey.core.util.ReaderWriter;

@Component
@Provider
@Produces({"text/plain", "*/*"})
public class AverageMessageBodyWriter implements MessageBodyWriter<Average> {

	@Override
	public boolean isWriteable(final Class<?> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType) {
		return type == Average.class;
	}

	@Override
	public long getSize(final Average t, final Class<?> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType) {
		return -1;
	}

	@Override
	public void writeTo(final Average average, final Class<?> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType, final MultivaluedMap<String, Object> httpHeaders,
		final OutputStream entityStream) throws IOException, WebApplicationException {
		final String content = average.getAverage().toString();
		ReaderWriter.writeToAsString(content, entityStream, mediaType);
	}

}
