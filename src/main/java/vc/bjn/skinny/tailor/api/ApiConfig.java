package vc.bjn.skinny.tailor.api;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.springframework.stereotype.Component;

@Provider
@Component
public class ApiConfig implements ContextResolver<ObjectMapper> {

	private ObjectMapper objectMapper;

	@Override
	public ObjectMapper getContext(final Class<?> type) {
		if(objectMapper == null){
			objectMapper = new ObjectMapper();
			applyConfig(objectMapper);
		}
		return objectMapper;
	}

	public static void applyConfig(final ObjectMapper om) {
		om.configure(Feature.WRITE_DATES_AS_TIMESTAMPS, true);
		om.setSerializationInclusion(Inclusion.ALWAYS);
	}

}
