package vc.bjn.skinny.tailor.api.util;

import javax.ws.rs.core.UriInfo;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class RequestUriInfoHolder {

	private UriInfo uriInfo;

	public UriInfo getUriInfo() {
		return uriInfo;
	}

	public void setUriInfo(final UriInfo uriInfo) {
		this.uriInfo = uriInfo;
	}
}
