package vc.bjn.skinny.tailor.api.resource;

import java.io.IOException;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import vc.bjn.skinny.tailor.data.query.ga.InterfaceBehaviorQuery;
import vc.bjn.skinny.tailor.data.query.ga.PlatformShareQuery;

@Path("attending")
@Component
@Scope("request")
@Consumes({"application/json"})
@Produces({"application/json"})
public class AttendingResource extends AbstractUriFilteredResource {

	static final Logger LOGGER = LoggerFactory.getLogger(AttendingResource.class);

	@Autowired private PlatformShareQuery browserShareQuery;
	@Autowired private InterfaceBehaviorQuery interfaceBehaviorQuery;

	@GET
	@Path("osShare")
	public Map<String, Integer> getOsShare() throws IOException{
		return browserShareQuery.getOsShare();
	}

	@GET
	@Path("browserShare")
	public Map<String, Integer> getBrowserShare() throws IOException{
		return browserShareQuery.getBrowserShare();
	}

	@GET
	@Path("platformSupportedRatio")
	public Map<String, Integer> getPlatformSupported() throws IOException {
		return browserShareQuery.getPlatformSupportedAndUnsupportedCount();
	}

	/*@GET
	@Path("avgUiLoadTime")
	public Double getAverageUiLoadTime() throws IOException {
		return interfaceBehaviorQuery.getAverageUILoadTime();
	}*/

	/*@GET
	@Path("uiLoadCount")
	public Integer getUILoadCount() throws IOException {
				return interfaceBehaviorQuery.getUILoadCount();
	}*/

	@GET
	@Path("avgJoinTime")
	public Double getAverageJoinTime() throws IOException {
		return interfaceBehaviorQuery.getAverageJoinTime();
	}


	/*@SuppressWarnings("unchecked")
	@GET
	@Path("joinCount")
	public Integer getJoinCount() throws IOException {
		return ((WarehouseQuery<Integer>) queryRegistry.get("/attending/joinCount")).getCached();
	}*/
}
