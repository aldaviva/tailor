package vc.bjn.skinny.tailor.api.resource;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;

import vc.bjn.skinny.tailor.api.util.RequestUriInfoHolder;
import vc.bjn.skinny.tailor.data.query.QueryLocator;
import vc.bjn.skinny.tailor.data.query.Query;
import vc.bjn.skinny.tailor.service.warehouse.QueryConfig;

public abstract class AbstractUriFilteredResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractUriFilteredResource.class);

	@Context protected HttpServletRequest request;

	@Autowired protected RequestUriInfoHolder requestUriInfoHolder;

	@Autowired protected QueryConfig queryRegistry;

	@Autowired private QueryLocator queryLocator;

	/*
	 * Must be public, not protected. Otherwise, Jersey won't invoke it and any query that needs a DateFilter will get a null pointer.
	 */
	@Context
	public final void setUriInfo(final UriInfo uriInfo){
		requestUriInfoHolder.setUriInfo(uriInfo);
	}

	@GET
	@Path("{metric}")
	public Object genericGet() {
		LOGGER.trace("Generic API request for {}", request.getPathInfo());

		try {
			final Query<?> warehouseQuery = queryLocator.getQuery(request.getPathInfo());
			return warehouseQuery.getCached();
		} catch (final NoSuchBeanDefinitionException e){
			throw new WebApplicationException(Response
				.status(Status.NOT_FOUND)
				.type(MediaType.TEXT_PLAIN_TYPE)
				.entity("Resource "+request.getPathInfo()+" not found in query registry.")
				.build());
		}

	}
}
