package vc.bjn.skinny.tailor.api.resource;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import vc.bjn.skinny.tailor.data.query.ga.InstallationQuery;

@Path("installation")
@Component
@Scope("request")
@Consumes({"application/json"})
@Produces({"application/json"})
public class InstallationResource extends AbstractUriFilteredResource {

	@Autowired private InstallationQuery installationQuery;

	@GET
	@Path("successAndFailureCount")
	public Map<String, Integer> getSuccessAndFailureCount() throws IOException{
		return installationQuery.getSuccessAndFailureCount();
	}

	@GET
	@Path("versionsInstalledOverTime")
	public Map<Date, Map<String, Integer>> getVersionsInstalledOverTime() throws IOException {
		return installationQuery.getVersionsInstalledOverTime();
	}

	/*@GET
	@Path("attempts")
	public Integer getAttempts() throws IOException {
			return installationQuery.getDownloadAttempts();
	}*/

	@GET
	@Path("manualTime")
	public Map<String, Map<String, Number>> getManualTime() throws IOException {
		return installationQuery.getManualTime();
	}

	@GET
	@Path("attemptsByMethod")
	public Map<String, Map<String, Integer>> getAttemptsByMethod() throws IOException {
		return installationQuery.getAttemptsByMethod();
	}

	@GET
	@Path("timeByMethod")
	public Map<String, Double> getTimeByMethod() throws IOException {
		return installationQuery.getTimeByUpdateMethod();
	}

}
