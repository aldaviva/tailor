package vc.bjn.skinny.tailor.api.resource;

import java.io.IOException;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import vc.bjn.skinny.tailor.data.query.ga.BehaviorQuery;

@Path("behavior")
@Component
@Scope("request")
@Consumes({"application/json"})
@Produces({"application/json"})
public class BehaviorResource extends AbstractUriFilteredResource {

	@Autowired private BehaviorQuery behaviorQuery;

	@GET
	@Path("moderationRatio")
	public Map<String, Integer> getModerationRatio() throws IOException{
		return behaviorQuery.getModerationRatio();
	}

	@GET
	@Path("authenticatedRatio")
	public Map<String, Integer> getAuthenticatedRatio() throws IOException{
		return behaviorQuery.getAuthenticatedRatio();
	}

	/*@GET
	@Path("elementUsage")
	public Map<String, Integer> getElementUsage() throws IOException{
		return behaviorQuery.getElementUsage();
	}*/

}
