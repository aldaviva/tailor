package vc.bjn.skinny.tailor.api.marshal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import com.sun.jersey.core.provider.AbstractMessageReaderWriterProvider;

@Component
@Provider
@Produces({"text/plain", "*/*"})
@Consumes({"text/plain", "*/*"})
public class JodaMessageReaderWriter extends AbstractMessageReaderWriterProvider<DateTime> {

	@Override
	public boolean isReadable(final Class<?> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType) {
		return type == DateTime.class;
	}

	@Override
	public DateTime readFrom(final Class<DateTime> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType, final MultivaluedMap<String, String> httpHeaders,
		final InputStream entityStream) throws IOException, WebApplicationException {
		return new DateTime(Long.parseLong(readFromAsString(entityStream, mediaType)));
	}

	@Override
	public boolean isWriteable(final Class<?> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType) {
		return isReadable(type, genericType, annotations, mediaType);
	}

	@Override
	public void writeTo(final DateTime t, final Class<?> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType, final MultivaluedMap<String, Object> httpHeaders,
		final OutputStream entityStream) throws IOException, WebApplicationException {
		writeToAsString(String.valueOf(t.getMillis()), entityStream, mediaType);
	}

}
