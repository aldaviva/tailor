package vc.bjn.skinny.tailor.api.resource;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vc.bjn.skinny.tailor.data.entity.Release;
import vc.bjn.skinny.tailor.data.entity.VersionMatrix;
import vc.bjn.skinny.tailor.data.provider.DevelopmentDataProvider;
import vc.bjn.skinny.tailor.service.LinesOfCodeService;

@Path("development")
@Component
@Consumes({"application/json"})
@Produces({"application/json"})
public class DevelopmentResource {

	@Autowired private DevelopmentDataProvider developmentDataProvider;
	@Autowired private LinesOfCodeService linesOfCodeService;

	@GET
	@Path("release")
	public Release getRelease(){
		return developmentDataProvider.getRelease();
	}

	@GET
	@Path("versionMatrix")
	public VersionMatrix getVersionMatrix(){
		return developmentDataProvider.getVersionMatrix();
	}

	@PUT
	@Path("release")
	public void updateRelease(final Map<String, Object> release){
		developmentDataProvider.updateRelease(release);
	}

	@PUT
	@Path("versionMatrix")
	public void updateVersionMatrix(final Map<String, Object> versionMatrix) {
		developmentDataProvider.updateVersionMatrix(versionMatrix);
	}

	@GET
	@Path("linesOfCode")
	public Map<String, Integer> getLinesOfCode(){
		final Map<String, Integer> result = new HashMap<>();

		result.put("js", linesOfCodeService.getLinesOfJavascriptCode());
		result.put("css", linesOfCodeService.getLinesOfCssCode());
		result.put("c++", linesOfCodeService.getLinesOfCppCode());

		return result;
	}

}
