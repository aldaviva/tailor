package vc.bjn.skinny.tailor.cache;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Aspect
@Component
@SuppressWarnings("unused")
public class QueryCachingAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(QueryCachingAspect.class);

	private static final boolean ENABLE_CACHING = true;
	private static final int CACHE_TTL_MINUTES = 60 * 60; //store results in cache for 1 hour each, because Google Analytics updates every 24 hours

	@Autowired
	@Qualifier("couchbaseCache")
	private Cache cache;


	@Pointcut("execution(public * *(..))")
	private void publicMethod(){}

	@Pointcut("within(vc.bjn.skinny.tailor.data.query.ga..*) || within(vc.bjn.skinny.tailor.service..*)")
	private void inPackages(){}

	@Pointcut("@annotation(vc.bjn.skinny.tailor.cache.Cacheable)")
	private void cachingMethod(){}

	@Pointcut("@target(vc.bjn.skinny.tailor.cache.Cacheable)")
	private void cachingClass(){}

	@Pointcut("execution(* getDateFilter(..)) || execution(* init(..)) || execution(* setUriInfo(..))")
	private void methodsToExclude(){}

	@Pointcut("publicMethod() && inPackages() && (cachingMethod() || cachingClass()) && !methodsToExclude()")
	protected void queryToCache(){}


	@Around("queryToCache()")
	public Object doCacheCheck(final ProceedingJoinPoint pjp) throws Throwable {
		final Object returnVal;

		if(ENABLE_CACHING){
			final String cacheKey = Cache.QUERY_PREFIX + pjp.getTarget().getClass().getSimpleName() + "." + pjp.getSignature().getName() + "(" + StringUtils.join(pjp.getArgs(), ", ").replaceAll("\\s", "_") + ")";

			LOGGER.debug("cache get {}", cacheKey);
			final Object cachedResult = cache.get(cacheKey);

			if(cachedResult == null){
				LOGGER.trace("cache miss {}", cacheKey);

				returnVal = pjp.proceed();

				LOGGER.trace("cache set {} = {}", cacheKey, returnVal);
				cache.set(cacheKey, returnVal, CACHE_TTL_MINUTES);
			} else {
				returnVal = cachedResult;
				LOGGER.trace("cache hit {} = {}", cacheKey, returnVal);
			}
		} else {
			returnVal = pjp.proceed();
		}

		return returnVal;
	}
}
