package vc.bjn.skinny.tailor.cache;

public interface Cache {

	public static final String QUERY_PREFIX = "cache:";
	public static final String WAREHOUSE_PREFIX = "warehouse-report:";
	public static final int NEVER_EXPIRES = 0;

	boolean has(String key);

	<T> T get(String key);

	<T> void set(String key, T value);

	<T> void set(String key, T value, int ttl);

	int incr(String key, int by);

}
