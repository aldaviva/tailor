package vc.bjn.skinny.tailor.cache;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Used as a marker by the QueryCachingAspect.
 *
 * Indicates that the marked method (or marked class's methods) should store
 * its result in the cache, and try to return cached values instead of
 * executing again.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Cacheable {

}
