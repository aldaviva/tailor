package vc.bjn.skinny.tailor.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.couchbase.client.CouchbaseClient;

@Component
public class CouchbaseCache implements Cache {

	private static final Logger LOGGER = LoggerFactory.getLogger(CouchbaseCache.class);

	@Autowired private CouchbaseClient couchbase;

	@Override
	public boolean has(final String key) {
		return (couchbase.get(key) != null);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(final String key) {
		final T result = (T) couchbase.get(key);
		LOGGER.trace("get(key={}, value={}, type={})", new Object[]{ key, result, (result == null ? "null" : result.getClass().getSimpleName()) });
		return result;
	}

	@Override
	public <T> void set(final String key, final T value) {
		set(key, value, NEVER_EXPIRES);
	}

	/**
	 * @param key
	 * @param value
	 * @param ttl minutes
	 */
	@Override
	public <T> void set(final String key, final T value, final int ttl) {
		couchbase.set(key, ttl, value);
		LOGGER.trace("set(key={}, value={}, ttl={})", new Object[]{ key, value, ttl });
	}

	/*
	 * Atomic, bitches
	 */
	@Override
	public int incr(final String key, final int by) {
		final long incrementedValue = couchbase.incr(key, by, by);
		return (int) incrementedValue;
	}

}
