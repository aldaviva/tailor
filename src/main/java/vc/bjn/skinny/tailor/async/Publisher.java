package vc.bjn.skinny.tailor.async;

public interface Publisher {

	void publish(String topic, Object message);

}
