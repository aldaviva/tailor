package vc.bjn.skinny.tailor.async;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

@Configuration
public class AsyncConfig {

	private @Value("${amqp.username}") String username;
	private @Value("${amqp.password}") String password;
	private @Value("${amqp.host}") String host;
	private @Value("${amqp.port}") int port;
	private @Value("${amqp.virtualHost}") String virtualHost;

	@Bean
	public Connection rabbit() throws IOException{
		final ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setUsername(username);
		connectionFactory.setPassword(password);
		connectionFactory.setVirtualHost(virtualHost);
		connectionFactory.setHost(host);
		connectionFactory.setPort(port);
		return connectionFactory.newConnection();
	}
}
