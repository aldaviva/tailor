package vc.bjn.skinny.tailor.async;

import java.util.HashMap;
import java.util.Map;

public class TopicTranslator {

	private TopicTranslator(){}

	private static final Map<String, String> translator = new HashMap<>();

	static {
		translator.put("behaviors.loadedInterface", "/attending/uiLoadCount");
		translator.put("behaviors.interfaceEvents.videoStarted", "/attending/joinCount");
		translator.put("installation.downloadStarted", "/installation/attempts");
		translator.put("behaviors.elements", "/behavior/elementUsage");
	}

	/*
	 * @param eventTopic AMQP topic from the Receiver: "events.behaviors.loadedInterface"
	 * @returns REST resource path: "/attending/uiLoadCount"
	 */
	public static String translate(String eventTopic){
		eventTopic = eventTopic.replace("events.", "");

		final String translatedTopic = getMatchedTopic(eventTopic);
		if(translatedTopic != null){
			return translatedTopic;
		} else {
			return '/'+eventTopic.replace('.', '/');
		}
	}

	private static String getMatchedTopic(final String topic){
		int dotIndex = topic.length();
		String topicSubstring;

		while(dotIndex != -1){
			topicSubstring = topic.substring(0, dotIndex);
			if(translator.containsKey(topicSubstring)){
				return translator.get(topicSubstring);
			}

			dotIndex = topic.lastIndexOf('.', dotIndex - 1);
		}
		return null;
	}

}
