package vc.bjn.skinny.tailor.async.comet;

import javax.annotation.PostConstruct;

import org.cometd.bayeux.server.BayeuxServer;
import org.cometd.bayeux.server.LocalSession;
import org.cometd.bayeux.server.ServerChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vc.bjn.skinny.tailor.async.Publisher;

@Component
public class BayeuxPublisher implements Publisher {

	private static final Logger LOGGER = LoggerFactory.getLogger(BayeuxPublisher.class);

	@Autowired private BayeuxServer bayeuxServer;

	private LocalSession bayeuxSession;

	@PostConstruct
	public void startLocalSession(){
		bayeuxSession = bayeuxServer.newLocalSession("publisher");
		bayeuxSession.handshake();
	}

	@Override
	public void publish(final String channelName, final Object message){
		LOGGER.debug("publish(channel = {}, message = {})", channelName, message);

		final ServerChannel bayeuxChannel = bayeuxServer.getChannel(channelName);
		if(bayeuxChannel != null){
			bayeuxChannel.publish(bayeuxSession, message, null);
			LOGGER.trace("Outgoing Bayeux message on {} with message {}", channelName, message);
		} else {
			LOGGER.trace("Suppressing message publishing on {} because there are no subscribers.", channelName);
		}
	}
}