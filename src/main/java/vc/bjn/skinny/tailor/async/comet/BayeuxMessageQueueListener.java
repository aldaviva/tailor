package vc.bjn.skinny.tailor.async.comet;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vc.bjn.skinny.tailor.async.BaseMessageQueueListener;
import vc.bjn.skinny.tailor.async.TopicTranslator;
import vc.bjn.skinny.tailor.data.entity.Average;
import vc.bjn.skinny.tailor.data.query.Query;
import vc.bjn.skinny.tailor.data.query.QueryLocator;
import vc.bjn.skinny.tailor.data.query.mongo.AveragingQuery;
import vc.bjn.skinny.tailor.data.query.mongo.CountingQuery;
import vc.bjn.skinny.tailor.data.query.mongo.LiveQuery;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Envelope;

@Component
public class BayeuxMessageQueueListener extends BaseMessageQueueListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(BayeuxMessageQueueListener.class);

	@Autowired private QueryLocator queryLocator;

	private final ObjectMapper objectMapper;

	public BayeuxMessageQueueListener(){
		objectMapper = new ObjectMapper();
	}

	@Override
	protected void handleDelivery(final String consumerTag, final Envelope envelope, final BasicProperties properties, final byte[] body) throws IOException {
		mqChannel.basicAck(envelope.getDeliveryTag(), false);

		final String amqpTopic = envelope.getRoutingKey();
		LOGGER.debug("Incoming AMQP message on {}", amqpTopic);

		final String resourceTopic = TopicTranslator.translate(amqpTopic);

		incrementQuery(amqpTopic, resourceTopic, body);
	}

	@SuppressWarnings("unchecked")
	private void incrementQuery(final String amqpTopic, final String resourceTopic, final byte[] body) {
		final Query<?> query = getQueryByResource(resourceTopic);
		if(query != null && query instanceof LiveQuery){
			if(resourceTopic.equals("/behavior/elementUsage")){
				final String incrKey = amqpTopic.substring(amqpTopic.lastIndexOf('.')+1);
				((LiveQuery<Map<String, Integer>>) query).incr(Collections.singletonMap(incrKey, 1));
			} else if(query instanceof CountingQuery) {
				((CountingQuery) query).incr(1);
			} else if(query instanceof AveragingQuery) {
				try {
					final Map<String, ?> bodyMap = objectMapper.readValue(body, Map.class);
					final Double value = (Double) bodyMap.get("value");
					((AveragingQuery) query).incr(new Average(value, 1));
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}

			LOGGER.debug("Message corresponds to live query {}. Incrementing and publishing.", query.getResource());
		} else {
			LOGGER.debug("No CountingLiveWarehouseQuery registered for {}", resourceTopic);
		}
	}

	protected Query<?> getQueryByResource(final String resource){
		return queryLocator.getQuery(resource);
	}

}
