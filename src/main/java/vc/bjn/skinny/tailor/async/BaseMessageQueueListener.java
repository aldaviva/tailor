package vc.bjn.skinny.tailor.async;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public abstract class BaseMessageQueueListener implements MessageQueueListener {

	public static final String EXCHANGE_NAME = "tailor";

	@Autowired protected Connection mqConnection;
	protected Channel mqChannel;

	@PostConstruct
	protected void init() throws IOException {
		mqChannel = mqConnection.createChannel();
		mqChannel.exchangeDeclare(EXCHANGE_NAME, "topic", true);
		initSubscribers();
	}

	protected void initSubscribers() throws IOException{
		final String queueName = mqChannel.queueDeclare().getQueue();
		mqChannel.queueBind(queueName, EXCHANGE_NAME, "events.#");

		mqChannel.basicConsume(queueName, new DefaultConsumer(mqChannel) {

			@Override
			public void handleDelivery(final String consumerTag, final Envelope envelope, final BasicProperties properties, final byte[] body) throws IOException {
				BaseMessageQueueListener.this.handleDelivery(consumerTag, envelope, properties, body);
			}
		});
	}

	/**
	 * Implementations must manually invoke mqChannel.basicAck()
	 */
	protected abstract void handleDelivery(final String consumerTag, final Envelope envelope, final BasicProperties properties, final byte[] body) throws IOException;

}