import org.junit.Assert;
import org.junit.Test;

import vc.bjn.skinny.tailor.async.TopicTranslator;

public class TopicTranslatorTest {

	@Test
	public void testNoMatchFoundSoFallbackReturned(){
		Assert.assertEquals(TopicTranslator.translate("foo"), "/foo");
	}

	@Test
	public void testExactMatch(){
		Assert.assertEquals(TopicTranslator.translate("behaviors.loadedInterface"), "/attending/uiLoadCount");
	}

	@Test
	public void testOneParentMatch(){
		Assert.assertEquals(TopicTranslator.translate("behaviors.loadedInterface.foo"), "/attending/uiLoadCount");
	}

	@Test
	public void testTwoParentMatch(){
		Assert.assertEquals(TopicTranslator.translate("behaviors.loadedInterface.foo.bar"), "/attending/uiLoadCount");
	}

}
